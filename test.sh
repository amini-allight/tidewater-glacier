#!/bin/sh
set -eu
verilator -Wno-ASCRANGE -o test -Irtl -Itest --trace --binary "test/$1_tb.v"
./obj_dir/test
gtkwave ./obj_dir/test.vcd
