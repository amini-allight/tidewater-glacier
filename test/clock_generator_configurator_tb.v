/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
`ifndef CLOCK_GENERATOR_CONFIGURATOR_TB_V
`define CLOCK_GENERATOR_CONFIGURATOR_TB_V

`timescale 10ns/1ns

`include "clock_generator_configurator.v"

module clock_generator_configurator_tb;

reg clock;
reg reset;

wire ready;
wire error;

wire clock_resetn;
wire scl;
wire sda;

always begin
    #1 clock = ~clock;
end

initial begin
    $dumpfile("obj_dir/test.vcd");
    $dumpvars();
    clock = 1;
    reset = 1;
    #10 reset = 0;
    #100000 $finish();
end

clock_generator_configurator clock_generator_configurator_inst(
    clock,
    reset,

    ready,
    error,

    clock_resetn,
    scl,
    sda
);

endmodule

`endif
