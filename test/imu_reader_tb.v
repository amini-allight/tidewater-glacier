/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
`ifndef IMU_READER_TB_V
`define IMU_READER_TB_V

`timescale 10ns/1ns

`include "imu_reader.v"

module imu_reader_tb;

reg clock;
reg reset;

reg data_requested;
wire [15:0] data[5:0];
wire error;

wire scl;
wire sda;

always begin
    #1 clock = ~clock;
end

initial begin
    $dumpfile("obj_dir/test.vcd");
    $dumpvars();
    clock = 1;
    reset = 1;
    data_requested = 1;
    #10 reset = 0;
    #100 data_requested = 0;
    #300000 $finish();
end

imu_reader imu_reader_inst(
    clock,
    reset,

    data_requested,
    data,
    error,
    
    scl,
    sda
);

endmodule

`endif
