/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
`ifndef ADJUSTMENT_READER_TB_V
`define ADJUSTMENT_READER_TB_V

`timescale 10ns/1ns

`include "adjustment_reader.v"

module adjustment_reader_tb;

reg clock;
reg reset;

reg data_requested;
wire [7:0] eye_relief;
wire [7:0] ipd;
wire error;

wire scl;
wire sda;

always begin
    #1 clock = ~clock;
end

initial begin
    $dumpfile("obj_dir/test.vcd");
    $dumpvars();
    clock = 1;
    reset = 1;
    data_requested = 1;
    #10 reset = 0;
    #100 data_requested = 0;
    #100000 $finish();
end

adjustment_reader adjustment_reader_inst(
    clock,
    reset,

    data_requested,
    eye_relief,
    ipd,
    error,

    scl,
    sda
);

endmodule

`endif
