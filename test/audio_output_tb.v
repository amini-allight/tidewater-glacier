/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
`ifndef AUDIO_OUTPUT_TB_V
`define AUDIO_OUTPUT_TB_V

`timescale 10ns/1ns

`include "audio_output.v"

module audio_output_tb;

reg clock;
reg reset;

reg [15:0] data[1:0];
reg data_ready;
wire ready;
wire error;

wire scl;
wire sda_l;
wire sda_r;

always begin
    #1 clock = ~clock;
end

integer i = 0;

always @ (posedge clock) begin
    if (ready) begin
        if (i == 2125) begin
            data[0] <= 16'b1010101010101010;
            data[1] <= 16'b0101010101010101;
            data_ready <= 1;
            i = 0;
        end else begin
            data_ready <= 0;
            i = i + 1;
        end
    end
end

initial begin
    $dumpfile("obj_dir/test.vcd");
    $dumpvars();
    clock = 1;
    reset = 1;
    data[0] = 0;
    data[1] = 0;
    data_ready = 0;
    #10 reset = 0;
    #100000 $finish();
end

audio_output audio_output_inst(
    clock,
    reset,

    data,
    data_ready,
    ready,
    error,

    scl,
    sda_l,
    sda_r
);

endmodule

`endif
