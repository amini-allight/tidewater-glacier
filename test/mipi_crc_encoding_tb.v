/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
`ifndef MIPI_CRC_ENCODING_TB_V
`define MIPI_CRC_ENCODING_TB_V

`include "common/colors.v"
`include "mipi_crc_encoder.v"
`include "mipi_crc_decoder.v"

module mipi_crc_encoding_tb;

integer i;

integer byte_index;
integer bit_index;

integer step_index;

reg clock;
reg reset;
reg [7:0] data_enc[15:0];
reg [7:0] data_dec[15:0];

reg data_valid_enc;
reg [7:0] data_input_enc;
reg data_valid_dec;
reg [7:0] data_input_dec;
wire [15:0] checksum_out;
reg [15:0] checksum_in;
wire error;

always begin
    #1 clock = ~clock;
end

initial begin
    $dumpfile("obj_dir/test.vcd");
    $dumpvars();
    clock = 1;
    reset = 1;

    for (i = 0; i < 16; i++) begin
        data_enc[i] = 8'($urandom_range(0, 255));
    end

    data_dec = data_enc;
    byte_index = $urandom_range(0, 15);
    bit_index = $urandom_range(0, 7);
    data_dec[byte_index][bit_index] = data_dec[byte_index][bit_index] ^ 1;

    #2 reset = 0;

    for (step_index = 0; step_index < 32; step_index++) begin
        #2 if (step_index < 16) begin
            data_valid_enc = 1;
            data_valid_dec = 0;
            data_input_enc = data_enc[step_index];
        end else begin
            if (step_index == 16) begin
                checksum_in = checksum_out;
            end
            data_valid_enc = 0;
            data_valid_dec = 1;
            data_input_dec = data_dec[step_index];
        end
    end

    #2 data_valid_dec = 0;

    #2 if (!error) begin
        `display_red("FAILURE: error not detected");
        $finish;
    end else begin
        `display_green("SUCCESS: error detected");
        $finish;
    end
end

mipi_crc_encoder encoder_inst(
    clock,
    reset,

    data_valid_enc,
    data_input_enc,

    checksum_out
);

mipi_crc_decoder decoder_inst(
    clock,
    reset,

    data_valid_dec,
    data_input_dec,
    checksum_in,

    error
);

endmodule

`endif
