/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
`ifndef MIPI_ECC_ENCODING_TB_V
`define MIPI_ECC_ENCODING_TB_V

`timescale 10ns/1ns

`include "common/colors.v"
`include "mipi_ecc_encoder.v"
`include "mipi_ecc_decoder.v"

module mipi_ecc_encoding_tb;

integer i;

reg clock;
reg reset;
reg [23:0] header_enc;
reg [23:0] header_dec;
wire [7:0] ecc;
wire [23:0] header_out;
wire error;

always begin
    #1 clock = ~clock;
end

initial begin
    $dumpfile("obj_dir/test.vcd");
    $dumpvars();
    clock = 1;
    reset = 1;
    header_enc = 24'($urandom_range(0, 16777215));
    header_dec = header_enc;
    i = $urandom_range(0, 7);
    header_dec[i] = header_dec[i] ^ 1;

    #2 reset = 0;
    #3 if (header_out != header_enc) begin
        `display_red("FAILURE: output data is not equal to input data");
        $finish;
    end else begin
        `display_green("SUCCESS: output data is equal to input data");
        $finish;
    end
end

mipi_ecc_encoder encoder_inst(
    clock,
    reset,

    header_enc,

    ecc
);

mipi_ecc_decoder decoder_inst(
    clock,
    reset,

    header_dec,
    ecc,

    header_out,
    error
);

endmodule

`endif
