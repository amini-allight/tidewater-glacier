/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
`ifndef COLORS_V
`define COLORS_V

`define display_red(_message) $write("%c[1;31m", 8'd27); $display(_message); $write("%c[0m", 8'd27);
`define display_green(_message) $write("%c[1;32m", 8'd27); $display(_message); $write("%c[0m", 8'd27);

`endif
