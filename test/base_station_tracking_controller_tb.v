/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
`ifndef BASE_STATION_TRACKING_CONTROLLER_TB_V
`define BASE_STATION_TRACKING_CONTROLLER_TB_V

`timescale 10ns/1ns

`include "base_station_tracking_controller.v"

module base_station_tracking_controller_tb;

reg clock;
reg reset;

wire [15:0] envelope;
wire [15:0] data;
wire ready;

wire [15:0] track_clock;
wire [15:0] track_data;

always begin
    #1 clock = ~clock;
end

initial begin
    $dumpfile("obj_dir/test.vcd");
    $dumpvars();
    clock = 1;
    reset = 1;
    #10 reset = 0;
    #100000 $finish();
end

base_station_tracking_controller base_station_tracking_controller_inst(
    clock,
    reset,

    envelope,
    data,
    ready,

    track_clock,
    track_data
);

endmodule

`endif
