/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
`ifndef AUDIO_INPUT_TB_V
`define AUDIO_INPUT_TB_V

`timescale 10ns/1ns

`include "audio_input.v"

module audio_input_tb;

reg clock;
reg reset;

reg data_requested;
wire [15:0] data[1:0];
wire error;

wire audio_chip_select;
wire audio_clock;
reg audio_data_l;
reg audio_data_r;

always begin
    #1 clock = ~clock;
end

integer i = 0;

always @ (posedge clock) begin
    if (i == 2125) begin
        data_requested <= 1;
        i = 0;
    end else begin
        data_requested <= 0;
        i = i + 1;
    end
end

always @ (negedge audio_clock) begin
    audio_data_l <= !audio_data_l;
    audio_data_r <= !audio_data_r;
end

initial begin
    $dumpfile("obj_dir/test.vcd");
    $dumpvars();
    clock = 1;
    reset = 1;
    data_requested = 0;
    audio_data_l = 0;
    audio_data_r = 0;
    #10 reset = 0;
    #100000 $finish();
end

audio_input audio_input_inst(
    clock,
    reset,

    data_requested,
    data,
    error,

    audio_chip_select,
    audio_clock,
    audio_data_l,
    audio_data_r
);

endmodule

`endif
