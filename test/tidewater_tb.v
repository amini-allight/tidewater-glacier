/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
`ifndef TIDEWATER_TB_V
`define TIDEWATER_TB_V

`timescale 1ns/100ps

`include "tidewater.v"

module tidewater_tb;

reg external_clock_p;
reg external_clock_n;

wire [15:0] track_clock;
wire [15:0] track_data;

wire screen_l_resetn;
wire [2:0] screen_l_aux;
wire screen_l_clock_a_p;
wire screen_l_clock_a_n;
wire [3:0] screen_l_data_a_p;
wire [3:0] screen_l_data_a_n;
wire screen_l_clock_b_p;
wire screen_l_clock_b_n;
wire [3:0] screen_l_data_b_p;
wire [3:0] screen_l_data_b_n;

wire screen_r_resetn;
wire [2:0] screen_r_aux;
wire screen_r_clock_a_p;
wire screen_r_clock_a_n;
wire [3:0] screen_r_data_a_p;
wire [3:0] screen_r_data_a_n;
wire screen_r_clock_b_p;
wire screen_r_clock_b_n;
wire [3:0] screen_r_data_b_p;
wire [3:0] screen_r_data_b_n;

wire camera_eye_l_power_en;
wire camera_eye_l_xclk;
wire camera_eye_l_scl;
wire camera_eye_l_sda;
wire camera_eye_l_clock_p;
wire camera_eye_l_clock_n;
wire [3:0] camera_eye_l_data_p;
wire [3:0] camera_eye_l_data_n;

wire camera_eye_r_power_en;
wire camera_eye_r_xclk;
wire camera_eye_r_scl;
wire camera_eye_r_sda;
wire camera_eye_r_clock_p;
wire camera_eye_r_clock_n;
wire [3:0] camera_eye_r_data_p;
wire [3:0] camera_eye_r_data_n;

wire camera_mouth_power_en;
wire camera_mouth_xclk;
wire camera_mouth_scl;
wire camera_mouth_sda;
wire camera_mouth_clock_p;
wire camera_mouth_clock_n;
wire [3:0] camera_mouth_data_p;
wire [3:0] camera_mouth_data_n;

wire camera_pass_l_power_en;
wire camera_pass_l_xclk;
wire camera_pass_l_scl;
wire camera_pass_l_sda;
wire camera_pass_l_clock_p;
wire camera_pass_l_clock_n;
wire [3:0] camera_pass_l_data_p;
wire [3:0] camera_pass_l_data_n;

wire camera_pass_r_power_en;
wire camera_pass_r_xclk;
wire camera_pass_r_scl;
wire camera_pass_r_sda;
wire camera_pass_r_clock_p;
wire camera_pass_r_clock_n;
wire [3:0] camera_pass_r_data_p;
wire [3:0] camera_pass_r_data_n;

wire audio_in_chip_select;
wire audio_in_clock;
wire audio_in_data_l;
wire audio_in_data_r;

wire audio_out_scl;
wire audio_out_sda_l;
wire audio_out_sda_r;

wire usb_pullup;
wire usb_data_p;
wire usb_data_n;

wire usb_orientation;
wire usb_open_transmit;

wire usb_pd_scl;
wire usb_pd_sda;

wire clock_resetn;
wire clock_scl;
wire clock_sda;

wire imu_scl;
wire imu_sda;

wire adjust_scl;
wire adjust_sda;

tidewater tidewater_inst(
    external_clock_p,
    external_clock_n,

    track_clock,
    track_data,

    screen_l_resetn,
    screen_l_aux,
    screen_l_clock_a_p,
    screen_l_clock_a_n,
    screen_l_data_a_p,
    screen_l_data_a_n,
    screen_l_clock_b_p,
    screen_l_clock_b_n,
    screen_l_data_b_p,
    screen_l_data_b_n,

    screen_r_resetn,
    screen_r_aux,
    screen_r_clock_a_p,
    screen_r_clock_a_n,
    screen_r_data_a_p,
    screen_r_data_a_n,
    screen_r_clock_b_p,
    screen_r_clock_b_n,
    screen_r_data_b_p,
    screen_r_data_b_n,

    camera_eye_l_power_en,
    camera_eye_l_xclk,
    camera_eye_l_scl,
    camera_eye_l_sda,
    camera_eye_l_clock_p,
    camera_eye_l_clock_n,
    camera_eye_l_data_p,
    camera_eye_l_data_n,

    camera_eye_r_power_en,
    camera_eye_r_xclk,
    camera_eye_r_scl,
    camera_eye_r_sda,
    camera_eye_r_clock_p,
    camera_eye_r_clock_n,
    camera_eye_r_data_p,
    camera_eye_r_data_n,

    camera_mouth_power_en,
    camera_mouth_xclk,
    camera_mouth_scl,
    camera_mouth_sda,
    camera_mouth_clock_p,
    camera_mouth_clock_n,
    camera_mouth_data_p,
    camera_mouth_data_n,

    camera_pass_l_power_en,
    camera_pass_l_xclk,
    camera_pass_l_scl,
    camera_pass_l_sda,
    camera_pass_l_clock_p,
    camera_pass_l_clock_n,
    camera_pass_l_data_p,
    camera_pass_l_data_n,

    camera_pass_r_power_en,
    camera_pass_r_xclk,
    camera_pass_r_scl,
    camera_pass_r_sda,
    camera_pass_r_clock_p,
    camera_pass_r_clock_n,
    camera_pass_r_data_p,
    camera_pass_r_data_n,

    audio_in_chip_select,
    audio_in_clock,
    audio_in_data_l,
    audio_in_data_r,

    audio_out_scl,
    audio_out_sda_l,
    audio_out_sda_r,

    usb_pullup,
    usb_data_p,
    usb_data_n,

    usb_orientation,
    usb_open_transmit,

    usb_pd_scl,
    usb_pd_sda,

    clock_resetn,
    clock_scl,
    clock_sda,

    imu_scl,
    imu_sda,

    adjust_scl,
    adjust_sda
);

initial begin
    external_clock_p = 0;
    external_clock_n = 1;
end

always begin
    // 500 MHz, flips every 1 ns
    #1 external_clock_p = ~external_clock_p;
    #1 external_clock_n = ~external_clock_n;
end

endmodule

`endif
