/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
`ifndef USB_128B132B_ENCODING_V
`define USB_128B132B_ENCODING_V

`timescale 10ns/1ns

`include "common/colors.v"
`include "usb_128b132b_encoder.v"
`include "usb_128b132b_decoder.v"

module usb_128b132b_encoding;

integer i;

reg clock;
reg reset;
reg training;

reg control_in;
reg [7:0] data_in[15:0];

wire [131:0] encoded_data;

wire control_out;
wire [1:0] error_out;
wire [7:0] data_out[15:0];

always begin
    #1 clock = ~clock;
end

initial begin
    $dumpfile("obj_dir/test.vcd");
    $dumpvars();
    clock = 1;
    reset = 1;
    training = 0;

    control_in = 0;
    for (i = 0; i < 16; i++) begin
        data_in[i] = 8'($urandom_range(0, 255));
    end

    #2 reset = 0;
    #32 if (data_in != data_out) begin
        `display_red("FAILURE: output data is not equal to input data");
        $finish;
    end else begin
        `display_green("SUCCESS: output data is equal to input data");
        $finish;
    end
end

usb_128b132b_encoder encoder_inst(
    clock,
    reset,

    training,

    control_in,
    data_in,

    encoded_data
);

usb_128b132b_decoder decoder_inst(
    clock,
    reset,

    training,

    encoded_data,

    control_out,
    error_out,
    data_out
);

endmodule

`endif
