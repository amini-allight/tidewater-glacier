#!/bin/sh
set -eu
export PATH=$PATH:$VIVADO_BIN_PATH
mkdir -p build
vivado -mode batch -source syn/synthesize.tcl
vivado -mode batch -source syn/implement.tcl
vivado -mode batch -source syn/generate.tcl
vivado -mode batch -source syn/program.tcl
