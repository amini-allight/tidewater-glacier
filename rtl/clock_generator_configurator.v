/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
`ifndef CLOCK_GENERATOR_CONFIGURATOR_V
`define CLOCK_GENERATOR_CONFIGURATOR_V

`include "constants.v"

module clock_generator_configurator(
    clock,
    reset,

    ready,
    error,

    clock_resetn,
    scl,
    sda
);

input wire clock;
input wire reset;

output reg ready;
output reg error;

output reg clock_resetn;
output reg scl;
inout wire sda;

parameter CLOCK_FREQ = 100000000;

localparam [3:0] RELEASE_RESET_STAGE = 0;
localparam [3:0] START_STAGE = 1;
localparam [3:0] ADDRESS_STAGE = 2;
localparam [3:0] WRITE_STAGE = 3;
localparam [3:0] ACK_STAGE = 4;
localparam [3:0] STOP_STAGE = 5;

localparam [2:0] FINAL_BIT_INDEX = 7;

localparam [8:0] CLOCK_DIVISION = 9'(CLOCK_FREQ
    / `I2C_FAST_CLOCK_FREQ / 2);

// Has the write flag appended to the end
localparam [0:7] CLOCK_GENERATOR_ADDRESS = 8'b10101000;

localparam [0:7] CONFIG_DATA[0:83] = {
    8'b00000000, 8'b00000000, 8'b00000001, 8'b10100001,
    8'b00000000, 8'b00000010, 8'b00000000, 8'b00000000,
    8'b00000000, 8'b00000100, 8'b00000000, 8'b00010011,
    8'b00000000, 8'b00000110, 8'b00000000, 8'b01100000,
    8'b00000000, 8'b00001000, 8'b00110000, 8'b11100000,
    8'b00000000, 8'b00001010, 8'b00000001, 8'b10011010,
    8'b00000000, 8'b00001100, 8'b00000000, 8'b00000000,
    8'b00000000, 8'b00001110, 8'b00000001, 8'b10011000,
    8'b00000000, 8'b00010000, 8'b00000000, 8'b00000000,
    8'b00000000, 8'b00010010, 8'b00011100, 8'b00000000,
    8'b00000000, 8'b00010100, 8'b00000000, 8'b00010000,
    8'b00000000, 8'b00010110, 8'b00000000, 8'b00000000,
    8'b00000000, 8'b00011000, 8'b00011100, 8'b00000000,
    8'b00000000, 8'b00011010, 8'b00000000, 8'b00010000,
    8'b00000000, 8'b00011100, 8'b00000000, 8'b00000000,
    8'b00000000, 8'b00011110, 8'b00011100, 8'b00000000,
    8'b00000000, 8'b00100000, 8'b00000000, 8'b00010000,
    8'b00000000, 8'b00100010, 8'b00000000, 8'b00000000,
    8'b00000000, 8'b00100100, 8'b00011100, 8'b00000000,
    8'b00000000, 8'b00100110, 8'b00000000, 8'b00010000,
    8'b00000000, 8'b00101000, 8'b00000000, 8'b00000000
};
localparam [6:0] CONFIG_DATA_BYTE_COUNT = 84;

integer i;

reg write;
reg write_value;

reg [3:0] stage;
reg [2:0] sub_stage;
reg go_next;
reg [3:0] next_stage;

reg [6:0] byte_index;

reg [8:0] clock_counter;

assign sda = write ? write_value : 1'bz;

task do_reset; begin
    ready <= 0;
    error <= 0;

    clock_resetn <= 0;
    scl <= 1;

    write <= 1;
    write_value <= 1;

    stage <= RELEASE_RESET_STAGE;
    sub_stage <= 0;
    go_next <= 0;
    next_stage <= STOP_STAGE;

    byte_index <= 0;

    clock_counter <= 0;
end endtask

task do_release_reset; begin
    clock_resetn <= 1;
    stage <= START_STAGE;
end endtask

task do_start; begin
    write_value <= 0;
    stage <= ADDRESS_STAGE;
end endtask

task do_address; begin
    if (!scl) begin
        write_value <= CLOCK_GENERATOR_ADDRESS[sub_stage];
        sub_stage <= sub_stage + 1;
        go_next <= (sub_stage == FINAL_BIT_INDEX);
    end else begin
        if (go_next) begin
            go_next <= 0;
            stage <= ACK_STAGE;
            next_stage <= WRITE_STAGE;
        end
    end

    scl <= !scl;
end endtask

task do_write; begin
    if (!scl) begin
        write_value <= CONFIG_DATA[byte_index][sub_stage];
        sub_stage <= sub_stage + 1;
        go_next <= (sub_stage == FINAL_BIT_INDEX);
    end else begin
        if (go_next) begin
            go_next <= 0;
            stage <= ACK_STAGE;
            next_stage <= ((byte_index + 1) % 4 == 0 ? STOP_STAGE : WRITE_STAGE);
            byte_index <= byte_index + 1;
        end
    end

    scl <= !scl;
end endtask

task do_ack; begin
    // We arrive here at scl = 0
    if (!scl) begin
        write <= 0;
        write_value <= 0;
    end else begin
        write <= 1;

        if (sda) begin
            error <= 1;
            stage <= STOP_STAGE;
        end else begin
            stage <= next_stage;
        end
    end

    scl <= !scl;
end endtask

task do_stop; begin
    // We arrive here at scl = 0
    if (scl) begin
        write_value <= 1;
        if (byte_index == CONFIG_DATA_BYTE_COUNT) begin
            ready <= 1;
        end else begin
            stage <= START_STAGE;
        end
    end else begin
        scl <= 1;
        write_value <= 0;
    end
end endtask

task do_main; begin
    if (clock_counter == CLOCK_DIVISION) begin
        case (stage)
        default: begin end
        RELEASE_RESET_STAGE: do_release_reset();
        START_STAGE: do_start();
        ADDRESS_STAGE: do_address();
        WRITE_STAGE: do_write();
        ACK_STAGE: do_ack();
        STOP_STAGE: do_stop();
        endcase

        clock_counter <= 0;
    end else begin
        clock_counter <= clock_counter + 1;
    end
end endtask

always @ (posedge clock) begin
    if (reset) begin
        do_reset();
    end else begin
        do_main();
    end
end

endmodule

`endif
