/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
`ifndef MIPI_CRC_ENCODER_V
`define MIPI_CRC_ENCODER_V

// Receives data for N clocks while data_valid is high, checksum is available
// on the next clock after the last data byte. data_valid must go low for at
// least one clock in between uses to allow the checksum to reset
module mipi_crc_encoder(
    clock,
    reset,

    data_valid,
    data,
    checksum
);

input wire clock;
input wire reset;

input wire data_valid;
input wire [7:0] data;
output reg [15:0] checksum;

`include "common/mipi_crc_encoding.v"

task do_reset; begin
    checksum <= 16'hffff;
end endtask

task do_main; begin
    if (data_valid) begin
        checksum <= generate_crc(checksum, data);
    end else begin
        checksum <= 16'hffff;
    end
end endtask

always @ (posedge clock) begin
    if (reset) begin
        do_reset();
    end else begin
        do_main();
    end
end

endmodule

`endif
