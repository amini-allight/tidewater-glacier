/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
`ifndef GTH_TRANSMITTER_V
`define GTH_TRANSMITTER_V

module gth_transmitter(
    clock,
    reset,

    tx_user_clock,
    tx_user_clock_2,
    tx_data
);

// TODO: USB: GTH transmitter

input wire clock;
input wire reset;

input wire tx_user_clock;
input wire tx_user_clock_2;
input wire [127:0] tx_data;

localparam USER_CLOCK_FREQ = 250000000;
localparam USER_CLOCK_FREQ_2 = 125000000;

wire [15:0] tx_control_0;
wire [15:0] tx_control_1;
wire tx_data_extend_reserved;

assign tx_control_0 = 0;
assign tx_control_1 = 0;
assign tx_data_extend_reserved = 0;

GTH_TX #(
    .TX_DATA_WIDTH(80), // input width
    .TX_INT_DATAWIDTH(1), // 4-byte internal data path for >8 Gb/s
    .TX_FABINT_USRCLK_FLOP(0) // ignored when 4-byte internal data path is used
) gth_tx_inst (
    .TXCTRL0(tx_control_0),
    .TXCTRL1(tx_control_1),
    .TXDATA(tx_data),
    .TXDATAEXTENDRSVD(tx_data_extend_reserved),
    .TXUSRCLK(tx_user_clock),
    .TXUSRCLK2(tx_user_clock_2)
);

task do_reset; begin

end endtask

task do_main; begin

end endtask

always @ (posedge clock) begin
    if (reset) begin
        do_reset();
    end else begin
        do_main();
    end
end

endmodule

`endif
