/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
`ifndef GTH_RECEIVER_V
`define GTH_RECEIVER_V

module gth_transmitter(
    clock,
    reset,

    rx_user_clock,
    rx_user_clock_2,
    rx_data
);

// TODO: USB: GTH receiver

input wire clock;
input wire reset;

input wire rx_user_clock;
input wire rx_user_clock_2;
output wire [127:0] rx_data;

localparam USER_CLOCK_FREQ = 250000000;
localparam USER_CLOCK_FREQ_2 = 125000000;

wire [15:0] rx_control_0;
wire [15:0] rx_control_1;
wire rx_data_extend_reserved;

GTH_RX #(
    .RX_DATA_WIDTH(80), // input width
    .RX_INT_DATAWIDTH(1), // 4-byte internal data path for >8 Gb/s
    .RX_FABINT_USRCLK_FLOP(0) // ignored when 4-byte internal data path is used
) gth_rx_inst (
    .RXCTRL0(rx_control_0),
    .RXCTRL1(rx_control_1),
    .RXDATA(rx_data),
    .RXDATAEXTENDRSVD(rx_data_extend_reserved),
    .RXUSRCLK(rx_user_clock),
    .RXUSRCLK2(rx_user_clock_2)
);

task do_reset; begin

end endtask

task do_main; begin

end endtask

always @ (posedge clock) begin
    if (reset) begin
        do_reset();
    end else begin
        do_main();
    end
end

endmodule

`endif
