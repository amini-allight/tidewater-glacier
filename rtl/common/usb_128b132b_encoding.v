/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
/* Adapted from Universal Serial Bus 3.2 Specification Section B.1 */

// TODO: USB: What are either of these defined as?
localparam [7:0] COMMA = 8'b0;
localparam [7:0] SKIP = 8'b1;

function [7:0] scramble(input [7:0] symbol); begin
    // TODO: USB: Something about KCODEs involving a 9th bit can also prevent encoding, I don't understand
    if (symbol == COMMA || symbol == SKIP || training) begin
        scramble = symbol;
    end else begin
        scramble[0] = symbol[0] ^ lfsr[15];
        scramble[1] = symbol[1] ^ lfsr[14];
        scramble[2] = symbol[2] ^ lfsr[13];
        scramble[3] = symbol[3] ^ lfsr[12];
        scramble[4] = symbol[4] ^ lfsr[11];
        scramble[5] = symbol[5] ^ lfsr[10];
        scramble[6] = symbol[6] ^ lfsr[9];
        scramble[7] = symbol[7] ^ lfsr[8];
    end
end endfunction

task do_advance; begin
    case (raw_symbols[sub_stage])
    COMMA: lfsr <= 16'hffff; // reset on COMMA
    SKIP: begin end // Don't advance on SKIP
    default: begin
        lfsr[ 0] <= lfsr[ 8];
        lfsr[ 1] <= lfsr[ 9];
        lfsr[ 2] <= lfsr[10];
        lfsr[ 3] <= lfsr[11] ^ lfsr[ 8];
        lfsr[ 4] <= lfsr[12] ^ lfsr[ 9] ^ lfsr[ 8];
        lfsr[ 5] <= lfsr[13] ^ lfsr[10] ^ lfsr[ 9] ^ lfsr[ 8];
        lfsr[ 6] <= lfsr[14] ^ lfsr[11] ^ lfsr[10] ^ lfsr[ 9];
        lfsr[ 7] <= lfsr[15] ^ lfsr[12] ^ lfsr[11] ^ lfsr[10];
        lfsr[ 8] <= lfsr[ 0] ^ lfsr[13] ^ lfsr[12] ^ lfsr[11];
        lfsr[ 9] <= lfsr[ 1] ^ lfsr[14] ^ lfsr[13] ^ lfsr[12];
        lfsr[10] <= lfsr[ 2] ^ lfsr[15] ^ lfsr[14] ^ lfsr[13];
        lfsr[11] <= lfsr[ 3] ^ lfsr[15] ^ lfsr[14];
        lfsr[12] <= lfsr[ 4] ^ lfsr[15];
        lfsr[13] <= lfsr[ 5];
        lfsr[14] <= lfsr[ 6];
        lfsr[15] <= lfsr[ 7];
    end
    endcase
end endtask
