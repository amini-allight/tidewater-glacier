/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */

function [15:0] generate_crc(input [15:0] crc, input [7:0] data); begin
    generate_crc[0] = crc[0] ^ crc[4] ^ crc[8] ^ data[0] ^ data[4];
    generate_crc[1] = crc[1] ^ crc[5] ^ crc[9] ^ data[1] ^ data[5];
    generate_crc[2] = crc[2] ^ crc[6] ^ crc[10] ^ data[2] ^ data[6];
    generate_crc[3] = crc[0] ^ crc[3] ^ crc[7] ^ crc[11] ^ data[0] ^ data[3] ^ data[7];
    generate_crc[4] = crc[1] ^ crc[12] ^ data[1];
    generate_crc[5] = crc[2] ^ crc[13] ^ data[2];
    generate_crc[6] = crc[3] ^ crc[14] ^ data[3];
    generate_crc[7] = crc[0] ^ crc[4] ^ crc[15] ^ data[0] ^ data[4];
    generate_crc[8] = crc[0] ^ crc[1] ^ crc[5] ^ data[0] ^ data[1] ^ data[5];
    generate_crc[9] = crc[1] ^ crc[2] ^ crc[6] ^ data[1] ^ data[2] ^ data[6];
    generate_crc[10] = crc[2] ^ crc[3] ^ crc[7] ^ data[2] ^ data[3] ^ data[7];
    generate_crc[11] = crc[3] ^ data[3];
    generate_crc[12] = crc[0] ^ crc[4] ^ data[0] ^ data[4];
    generate_crc[13] = crc[1] ^ crc[5] ^ data[1] ^ data[5];
    generate_crc[14] = crc[2] ^ crc[6] ^ data[2] ^ data[6];
    generate_crc[15] = crc[3] ^ crc[7] ^ data[3] ^ data[7];
end endfunction
