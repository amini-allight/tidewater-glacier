/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */

function [7:0] calculate_ecc(input [23:0] header); begin
    calculate_ecc[7] = 0;
    calculate_ecc[6] = 0;
    calculate_ecc[5] = header[10] ^ header[11] ^ header[12] ^ header[13] ^ header[14] ^ header[15] ^ header[16] ^ header[17] ^ header[18] ^ header[19] ^ header[21] ^ header[22] ^ header[23];
    calculate_ecc[4] = header[4] ^ header[5] ^ header[6] ^ header[7] ^ header[8] ^ header[9] ^ header[16] ^ header[17] ^ header[18] ^ header[19] ^ header[20] ^ header[22] ^ header[23];
    calculate_ecc[3] = header[1] ^ header[2] ^ header[3] ^ header[7] ^ header[8] ^ header[9] ^ header[13] ^ header[14] ^ header[15] ^ header[19] ^ header[20] ^ header[21] ^ header[23];
    calculate_ecc[2] = header[0] ^ header[2] ^ header[3] ^ header[5] ^ header[6] ^ header[9] ^ header[11] ^ header[12] ^ header[15] ^ header[18] ^ header[20] ^ header[21] ^ header[2];
    calculate_ecc[1] = header[0] ^ header[1] ^ header[3] ^ header[4] ^ header[6] ^ header[8] ^ header[10] ^ header[12] ^ header[14] ^ header[17] ^ header[20] ^ header[21] ^ header[22] ^ header[23];
    calculate_ecc[0] = header[0] ^ header[1] ^ header[2] ^ header[4] ^ header[5] ^ header[7] ^ header[10] ^ header[11] ^ header[13] ^ header[16] ^ header[20] ^ header[21] ^ header[22] ^ header[23];
end endfunction
