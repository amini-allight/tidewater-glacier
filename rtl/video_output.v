/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
`ifndef VIDEO_OUTPUT_V
`define VIDEO_OUTPUT_V

`include "constants.v"
`include "simulation/obufds_dphy.v"
`include "mipi_hs_transmitter.v"

module video_output(
    logic_clock,
    mipi_dsi_clock,
    reset,

    scanline,
    enabled,
    ready,
    error,

    screen_resetn,
    screen_aux,
    mipi_clock_a_p,
    mipi_clock_a_n,
    mipi_data_a_p,
    mipi_data_a_n,
    mipi_clock_b_p,
    mipi_clock_b_n,
    mipi_data_b_p,
    mipi_data_b_n
);

input wire logic_clock;
input wire mipi_dsi_clock;
input wire reset;

input wire [SCANLINE_SIZE - 1:0] scanline;
input wire enabled;
output reg ready;
output reg error;

output reg screen_resetn;
inout wire [2:0] screen_aux;
output wire mipi_clock_a_p;
output wire mipi_clock_a_n;
output wire [3:0] mipi_data_a_p;
output wire [3:0] mipi_data_a_n;
output wire mipi_clock_b_p;
output wire mipi_clock_b_n;
output wire [3:0] mipi_data_b_p;
output wire [3:0] mipi_data_b_n;

parameter LOGIC_CLOCK_FREQ = 122400000;
parameter MIPI_DSI_CLOCK_FREQ = 500000000;
parameter SCANLINE_SIZE = 1920 * 3 * 8;

localparam HS_TX_BYTE_WIDTH = 16;

localparam [2:0] INIT_STAGE = 0;
localparam [2:0] HS_REQUEST_STAGE = 1;
localparam [2:0] BRIDGE_STAGE = 2;
localparam [2:0] HS_SWITCHOVER_STAGE = 3;
localparam [2:0] HS_SYNC_STAGE = 4;
localparam [2:0] READY_STAGE = 5;

localparam [23:0] INIT_DELAY_COUNT = 24'($rtoi((10.0 / 1000.0) * LOGIC_CLOCK_FREQ));
localparam [23:0] HS_REQUEST_DELAY_COUNT = 24'($rtoi((50.0 / 1000000000.0) * LOGIC_CLOCK_FREQ)); // T(LPX)
localparam [23:0] BRIDGE_DELAY_COUNT = 24'($rtoi((60.0 / 1000000000.0) * LOGIC_CLOCK_FREQ)); // T(HS-PREPARE)
localparam [23:0] HS_SWITCHOVER_DELAY_COUNT = 24'($rtoi((90.0 / 1000000000.0) * LOGIC_CLOCK_FREQ)); // T(HS-ZERO)

localparam [14:0] FRAME_DIVISION = 15'(LOGIC_CLOCK_FREQ / `SCREEN_FRAME_RATE);

integer i;

reg high_speed;
reg seek_start;
reg [2:0] stage;
reg [23:0] counter;
reg should_transmit;
reg [14:0] frame_counter;

reg lp_clock_a_p;
reg lp_clock_a_n;
reg [3:0] lp_data_a_p;
reg [3:0] lp_data_a_n;
reg lp_clock_b_p;
reg lp_clock_b_n;
reg [3:0] lp_data_b_p;
reg [3:0] lp_data_b_n;

wire hs_clock_a;
wire [3:0] hs_data_a;
wire hs_clock_b;
wire [3:0] hs_data_b;

reg [7:0] hs_data_a_tx[HS_TX_BYTE_WIDTH - 1:0];
reg [7:0] hs_data_b_tx[HS_TX_BYTE_WIDTH - 1:0];

OBUFDS_DPHY clock_a_obufds(
    .HSTX_I(hs_clock_a),
    .HSTX_T(!high_speed),
    .LPTX_I_N(lp_clock_a_n),
    .LPTX_I_P(lp_clock_a_p),
    .LPTX_T(high_speed),
    .O(mipi_clock_a_p),
    .OB(mipi_clock_a_n)
);

OBUFDS_DPHY data_a_obufds[3:0](
    .HSTX_I(hs_data_a),
    .HSTX_T(!high_speed),
    .LPTX_I_N(lp_data_a_n),
    .LPTX_I_P(lp_data_a_p),
    .LPTX_T(high_speed),
    .O(mipi_data_a_p),
    .OB(mipi_data_a_n)
);

OBUFDS_DPHY clock_b_obufds(
    .HSTX_I(hs_clock_b),
    .HSTX_T(!high_speed),
    .LPTX_I_N(lp_clock_b_n),
    .LPTX_I_P(lp_clock_b_p),
    .LPTX_T(high_speed),
    .O(mipi_clock_b_p),
    .OB(mipi_clock_b_n)
);

OBUFDS_DPHY data_b_obufds[3:0](
    .HSTX_I(hs_data_b),
    .HSTX_T(!high_speed),
    .LPTX_I_N(lp_data_b_n),
    .LPTX_I_P(lp_data_b_p),
    .LPTX_T(high_speed),
    .O(mipi_data_b_p),
    .OB(mipi_data_b_n)
);

mipi_hs_transmitter #(
    .BYTE_WIDTH(HS_TX_BYTE_WIDTH)
) transmitter_a_inst(
    mipi_dsi_clock,
    reset,

    high_speed,
    seek_start,
    hs_data_a_tx,

    hs_clock_a,
    hs_data_a
);

mipi_hs_transmitter #(
    .BYTE_WIDTH(HS_TX_BYTE_WIDTH)
) transmitter_b_inst(
    mipi_dsi_clock,
    reset,

    high_speed,
    seek_start,
    hs_data_b_tx,

    hs_clock_b,
    hs_data_b
);

assign screen_aux = 3'b0;

task cycle_lp_clock; begin
    lp_clock_a_p <= !lp_clock_a_p;
    lp_clock_a_n <= !lp_clock_a_n;
    lp_clock_b_p <= !lp_clock_b_p;
    lp_clock_b_n <= !lp_clock_b_n;
end endtask

task do_reset; begin
    screen_resetn <= 0;

    lp_clock_a_p <= 0;
    lp_clock_a_n <= 1;
    lp_data_a_p <= 4'b1111;
    lp_data_a_n <= 4'b1111;
    lp_clock_b_p <= 0;
    lp_clock_b_n <= 1;
    lp_data_b_p <= 4'b1111;
    lp_data_b_n <= 4'b1111;

    for (i = 0; i < HS_TX_BYTE_WIDTH; i++) begin
        hs_data_a_tx[i] <= 0;
        hs_data_b_tx[i] <= 0;
    end

    high_speed <= 0;
    seek_start <= 0;
    stage <= INIT_STAGE;
    counter <= 0;
    should_transmit <= 1;
    frame_counter <= 0;
end endtask

task do_init; begin
    screen_resetn <= 1;
    lp_data_a_p <= 4'b1111;
    lp_data_a_n <= 4'b1111;
    lp_data_b_p <= 4'b1111;
    lp_data_b_n <= 4'b1111;

    if (counter == INIT_DELAY_COUNT) begin
        stage <= HS_REQUEST_STAGE;
        counter <= 0;
    end else begin
        counter <= counter + 1;
    end
end endtask

task do_hs_request; begin
    lp_data_a_p <= 4'b0000;
    lp_data_a_n <= 4'b1111;
    lp_data_b_p <= 4'b0000;
    lp_data_b_n <= 4'b1111;

    if (counter == HS_REQUEST_DELAY_COUNT) begin
        stage <= BRIDGE_STAGE;
        counter <= 0;
    end else begin
        counter <= counter + 1;
    end
end endtask

task do_bridge; begin
    lp_data_a_p <= 4'b0000;
    lp_data_a_n <= 4'b0000;
    lp_data_b_p <= 4'b0000;
    lp_data_b_n <= 4'b0000;

    if (counter == BRIDGE_DELAY_COUNT) begin
        stage <= HS_SWITCHOVER_STAGE;
        counter <= 0;
    end else begin
        counter <= counter + 1;
    end
end endtask

task do_hs_switchover; begin
    high_speed <= 1;
    for (i = 0; i < HS_TX_BYTE_WIDTH; i++) begin
        hs_data_a_tx[i] <= 0;
        hs_data_b_tx[i] <= 0;
    end

    if (counter == HS_SWITCHOVER_DELAY_COUNT) begin
        stage <= HS_SYNC_STAGE;
        counter <= 0;
    end else begin
        counter <= counter + 1;
    end
end endtask

task do_hs_sync; begin
    case (counter)
    0: begin
        seek_start <= 1;
        hs_data_a_tx[0] <= `MIPI_HS_SYNC_SEQUENCE;
        hs_data_b_tx[0] <= `MIPI_HS_SYNC_SEQUENCE;
        counter <= 1;
    end
    1: begin
        seek_start <= 0;
        counter <= 2;
    end
    2: begin
        stage <= READY_STAGE;
        hs_data_a_tx[0] <= 0;
        hs_data_b_tx[0] <= 0;
        counter <= 0;
    end
    default: begin end
    endcase
end endtask

task do_transmit; begin
    if (enabled && should_transmit) begin

    end

    if (frame_counter == FRAME_DIVISION) begin
        should_transmit <= 1;
        frame_counter <= 0;
    end else begin
        frame_counter <= frame_counter + 1;
    end
end endtask

task do_main; begin
    case (stage)
    default: begin end
    INIT_STAGE: do_init();
    HS_REQUEST_STAGE: do_hs_request();
    BRIDGE_STAGE: do_bridge();
    HS_SWITCHOVER_STAGE: do_hs_switchover();
    HS_SYNC_STAGE: do_hs_sync();
    READY_STAGE: do_transmit();
    endcase

    if (!high_speed) begin
        cycle_lp_clock();
    end
end endtask

always @ (posedge logic_clock) begin
    if (reset) begin
        do_reset();
    end else begin
        do_main();
    end
end

endmodule

`endif
