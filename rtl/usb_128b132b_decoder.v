/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
`ifndef USB_128B132B_DECODER_V
`define USB_128B132B_DECODER_V

`include "constants.v"

module usb_128b132b_decoder(
    clock,
    reset,

    training,

    encoded_data,

    control,
    error,
    raw_symbols
);

input wire clock;
input wire reset;

input wire training;

input wire [131:0] encoded_data;

output reg control;
output reg [1:0] error;
output reg [7:0] raw_symbols[15:0];

localparam BYTE_COUNT = 16;

integer i;

wire [3:0] preamble;
wire [7:0] encoded_symbols[15:0];

reg [15:0] lfsr;
reg [3:0] sub_stage;

assign preamble = encoded_data[3:0];
generate for (genvar i = 0; i < BYTE_COUNT; i++) begin
    assign raw_symbols[i] = encoded_data[((i + 1) * 8 + 4) - 1:i * 8 + 4];
end endgenerate

`include "common/usb_128b132b_encoding.v"

task do_reset; begin
    control <= 0;
    error <= `USB_128B132B_OK;
    for (i = 0; i < BYTE_COUNT; i++) begin
        raw_symbols[i] <= 0;
    end

    lfsr <= 16'hffff;
    sub_stage <= 0;
end endtask

task do_main; begin
    if (sub_stage == 0) begin
        case (preamble)
        `USB_128B132B_CONTROL_FLAG: begin
            control <= 1;
            error <= `USB_128B132B_OK;
        end
        `USB_128B132B_DATA_FLAG: begin
            control <= 0;
            error <= `USB_128B132B_OK;
        end
        4'b0000: begin
            error <= `USB_128B132B_TWO_BIT_ERROR;
        end
        4'b0001: begin
            error <= `USB_128B132B_ONE_BIT_ERROR;
        end
        4'b0010: begin
            error <= `USB_128B132B_ONE_BIT_ERROR;
        end
        4'b0100: begin
            error <= `USB_128B132B_ONE_BIT_ERROR;
        end
        4'b0101: begin
            error <= `USB_128B132B_TWO_BIT_ERROR;
        end
        4'b0110: begin
            error <= `USB_128B132B_TWO_BIT_ERROR;
        end
        4'b0111: begin
            error <= `USB_128B132B_ONE_BIT_ERROR;
        end
        4'b1000: begin
            error <= `USB_128B132B_ONE_BIT_ERROR;
        end
        4'b1001: begin
            error <= `USB_128B132B_TWO_BIT_ERROR;
        end
        4'b1010: begin
            error <= `USB_128B132B_TWO_BIT_ERROR;
        end
        4'b1011: begin
            error <= `USB_128B132B_ONE_BIT_ERROR;
        end
        4'b1101: begin
            error <= `USB_128B132B_ONE_BIT_ERROR;
        end
        4'b1110: begin
            error <= `USB_128B132B_ONE_BIT_ERROR;
        end
        4'b1111: begin
            error <= `USB_128B132B_TWO_BIT_ERROR;
        end
        endcase
    end

    raw_symbols[sub_stage] <= scramble(encoded_symbols[sub_stage]);
    sub_stage <= sub_stage + 1;

    do_advance();
end endtask

always @ (posedge clock) begin
    if (reset) begin
        do_reset();
    end else begin
        do_main();
    end
end

endmodule

`endif
