/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
`ifndef AUDIO_INPUT_V
`define AUDIO_INPUT_V

`include "constants.v"

// Conversion begins when data_requested goes high, result will be available
// 39 clocks later plus sync delay (@ 17 MHz) and remain available until
// data_requested is next signalled, which should happen at a rate of 48000 Hz
module audio_input(
    clock,
    reset,

    data_requested,
    data,
    error,

    audio_chip_select,
    audio_clock,
    audio_data_l,
    audio_data_r
);

input wire clock;
input wire reset;

input wire data_requested;
output reg [`AUDIO_BIT_DEPTH - 1:0] data[`AUDIO_CHANNELS - 1:0];
output reg error;

output reg audio_chip_select;
output reg audio_clock;
input wire audio_data_l;
input wire audio_data_r;

parameter CLOCK_FREQ = 122400000;

localparam [2:0] IDLE_STAGE = 0;
localparam [2:0] START_STAGE = 1;
localparam [2:0] WAIT_STAGE = 2;
localparam [2:0] ACK_STAGE = 3;
localparam [2:0] BIG_ENDIAN_STAGE = 4;
localparam [2:0] LITTLE_ENDIAN_STAGE = 5;
localparam [2:0] STOP_STAGE = 6;

localparam BUS_CLOCK_FREQ = 15300000;

localparam [2:0] CLOCK_DIVISION = 3'(CLOCK_FREQ / BUS_CLOCK_FREQ / 2);

integer i;

reg [2:0] stage;
reg [4:0] sub_stage;

reg [2:0] clock_counter;

task do_reset; begin
    for (i = 0; i < `AUDIO_CHANNELS; i = i + 1) begin
        data[i] <= 0;
    end
    error <= 0;
    
    audio_chip_select <= 1;
    audio_clock <= 0;

    stage <= IDLE_STAGE;
    sub_stage <= 0;

    clock_counter <= 0;
end endtask

task do_start; begin
    audio_chip_select <= 0;
    stage <= WAIT_STAGE;
end endtask

task do_wait; begin
    if (!audio_clock) begin
        if (sub_stage == 5) begin
            stage <= ACK_STAGE;
            sub_stage <= 0;
        end
    end else begin
        sub_stage <= sub_stage + 1;
    end

    audio_clock <= !audio_clock;
end endtask

task do_ack; begin
    if (!audio_clock) begin
        if (audio_data_l || audio_data_r) begin
            stage <= BIG_ENDIAN_STAGE;
            error <= 1;
        end else begin
            stage <= BIG_ENDIAN_STAGE;
        end
    end

    audio_clock <= !audio_clock;
end endtask

task do_big_endian; begin
    if (!audio_clock) begin
        if (sub_stage == `AUDIO_BIT_DEPTH) begin
            stage <= LITTLE_ENDIAN_STAGE;
            sub_stage <= 0;
        end
    end else begin
        sub_stage <= sub_stage + 1;
    end

    audio_clock <= !audio_clock;
end endtask

task do_little_endian; begin
    if (!audio_clock) begin
        data[0][sub_stage - 1] <= audio_data_l;
        data[1][sub_stage - 1] <= audio_data_r;
        if (sub_stage == `AUDIO_BIT_DEPTH) begin
            stage <= STOP_STAGE;
            sub_stage <= 0;
        end
    end else begin
        sub_stage <= sub_stage + 1;
    end

    audio_clock <= !audio_clock;
end endtask

task do_stop; begin
    audio_chip_select <= 1;
    audio_clock <= 0;
    stage <= IDLE_STAGE;
end endtask

task do_transmit; begin
    if (clock_counter == CLOCK_DIVISION) begin
        case (stage)
        default: begin end
        START_STAGE: do_start();
        WAIT_STAGE: do_wait();
        ACK_STAGE: do_ack();
        BIG_ENDIAN_STAGE: do_big_endian();
        LITTLE_ENDIAN_STAGE: do_little_endian();
        STOP_STAGE: do_stop();
        endcase

        clock_counter <= 0;
    end else begin
        clock_counter <= clock_counter + 1;
    end
end endtask

task do_main; begin
    if (data_requested) begin
        if (stage == IDLE_STAGE) begin
            stage <= START_STAGE;
        end
    end else begin
        do_transmit();
    end
end endtask

always @ (posedge clock) begin
    if (reset) begin
        do_reset();
    end else begin
        do_main();
    end
end

endmodule

`endif
