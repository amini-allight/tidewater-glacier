/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
`ifndef TIDEWATER_V
`define TIDEWATER_V

`include "startup.v"
`include "clock_generator_configurator.v"
`include "clock_manager.v"
`include "video_input.v"
`include "video_output.v"
`include "audio_input.v"
`include "audio_output.v"
`include "base_station_tracking_controller.v"
`include "imu_controller.v"
`include "adjustment_controller.v"
`include "usb_device.v"
`include "usb_pd_placeholder.v"
`include "audio_controller.v"
`include "controller.v"

module tidewater(
    external_clock_p,
    external_clock_n,

    track_clock,
    track_data,

    screen_l_resetn,
    screen_l_aux,
    screen_l_clock_a_p,
    screen_l_clock_a_n,
    screen_l_data_a_p,
    screen_l_data_a_n,
    screen_l_clock_b_p,
    screen_l_clock_b_n,
    screen_l_data_b_p,
    screen_l_data_b_n,

    screen_r_resetn,
    screen_r_aux,
    screen_r_clock_a_p,
    screen_r_clock_a_n,
    screen_r_data_a_p,
    screen_r_data_a_n,
    screen_r_clock_b_p,
    screen_r_clock_b_n,
    screen_r_data_b_p,
    screen_r_data_b_n,

    camera_eye_l_power_en,
    camera_eye_l_xclk,
    camera_eye_l_scl,
    camera_eye_l_sda,
    camera_eye_l_clock_p,
    camera_eye_l_clock_n,
    camera_eye_l_data_p,
    camera_eye_l_data_n,

    camera_eye_r_power_en,
    camera_eye_r_xclk,
    camera_eye_r_scl,
    camera_eye_r_sda,
    camera_eye_r_clock_p,
    camera_eye_r_clock_n,
    camera_eye_r_data_p,
    camera_eye_r_data_n,

    camera_mouth_power_en,
    camera_mouth_xclk,
    camera_mouth_scl,
    camera_mouth_sda,
    camera_mouth_clock_p,
    camera_mouth_clock_n,
    camera_mouth_data_p,
    camera_mouth_data_n,

    camera_pass_l_power_en,
    camera_pass_l_xclk,
    camera_pass_l_scl,
    camera_pass_l_sda,
    camera_pass_l_clock_p,
    camera_pass_l_clock_n,
    camera_pass_l_data_p,
    camera_pass_l_data_n,

    camera_pass_r_power_en,
    camera_pass_r_xclk,
    camera_pass_r_scl,
    camera_pass_r_sda,
    camera_pass_r_clock_p,
    camera_pass_r_clock_n,
    camera_pass_r_data_p,
    camera_pass_r_data_n,

    audio_in_chip_select,
    audio_in_clock,
    audio_in_data_l,
    audio_in_data_r,

    audio_out_scl,
    audio_out_sda_l,
    audio_out_sda_r,

    usb_pullup,
    usb_data_p,
    usb_data_n,

    usb_orientation,
    usb_open_transmit,

    usb_pd_scl,
    usb_pd_sda,

    clock_resetn,
    clock_scl,
    clock_sda,

    imu_scl,
    imu_sda,

    adjust_scl,
    adjust_sda
);

input wire external_clock_p;
input wire external_clock_n;

inout wire [`TRACK_SENSOR_COUNT - 1:0] track_clock;
inout wire [`TRACK_SENSOR_COUNT - 1:0] track_data;

output wire screen_l_resetn;
output wire [2:0] screen_l_aux;
output wire screen_l_clock_a_p;
output wire screen_l_clock_a_n;
output wire [3:0] screen_l_data_a_p;
output wire [3:0] screen_l_data_a_n;
output wire screen_l_clock_b_p;
output wire screen_l_clock_b_n;
output wire [3:0] screen_l_data_b_p;
output wire [3:0] screen_l_data_b_n;

output wire screen_r_resetn;
output wire [2:0] screen_r_aux;
output wire screen_r_clock_a_p;
output wire screen_r_clock_a_n;
output wire [3:0] screen_r_data_a_p;
output wire [3:0] screen_r_data_a_n;
output wire screen_r_clock_b_p;
output wire screen_r_clock_b_n;
output wire [3:0] screen_r_data_b_p;
output wire [3:0] screen_r_data_b_n;

output wire camera_eye_l_power_en;
output wire camera_eye_l_xclk;
output wire camera_eye_l_scl;
inout wire camera_eye_l_sda;
input wire camera_eye_l_clock_p;
input wire camera_eye_l_clock_n;
inout wire [3:0] camera_eye_l_data_p;
inout wire [3:0] camera_eye_l_data_n;

output wire camera_eye_r_power_en;
output wire camera_eye_r_xclk;
output wire camera_eye_r_scl;
inout wire camera_eye_r_sda;
input wire camera_eye_r_clock_p;
input wire camera_eye_r_clock_n;
inout wire [3:0] camera_eye_r_data_p;
inout wire [3:0] camera_eye_r_data_n;

output wire camera_mouth_power_en;
output wire camera_mouth_xclk;
output wire camera_mouth_scl;
inout wire camera_mouth_sda;
input wire camera_mouth_clock_p;
input wire camera_mouth_clock_n;
inout wire [3:0] camera_mouth_data_p;
inout wire [3:0] camera_mouth_data_n;

output wire camera_pass_l_power_en;
output wire camera_pass_l_xclk;
output wire camera_pass_l_scl;
inout wire camera_pass_l_sda;
input wire camera_pass_l_clock_p;
input wire camera_pass_l_clock_n;
inout wire [3:0] camera_pass_l_data_p;
inout wire [3:0] camera_pass_l_data_n;

output wire camera_pass_r_power_en;
output wire camera_pass_r_xclk;
output wire camera_pass_r_scl;
inout wire camera_pass_r_sda;
input wire camera_pass_r_clock_p;
input wire camera_pass_r_clock_n;
inout wire [3:0] camera_pass_r_data_p;
inout wire [3:0] camera_pass_r_data_n;

output wire audio_in_chip_select;
output wire audio_in_clock;
input wire audio_in_data_l;
input wire audio_in_data_r;

output wire audio_out_scl;
inout wire audio_out_sda_l;
inout wire audio_out_sda_r;

output wire usb_pullup;
inout wire usb_data_p;
inout wire usb_data_n;

input wire usb_orientation;
output wire usb_open_transmit;

output wire usb_pd_scl;
inout wire usb_pd_sda;

output wire clock_resetn;
output wire clock_scl;
inout wire clock_sda;

output wire imu_scl;
inout wire imu_sda;

output wire adjust_scl;
inout wire adjust_sda;

wire configuration_clock;

startup #(
    .CONFIG_CLOCK_FREQ(`CONFIG_CLOCK_FREQ)
) startup_inst(
    configuration_clock
);

reg reset_done = 0;
reg clock_generator_configurator_reset;
reg reset;

always @ (posedge configuration_clock) begin
    if (!reset_done) begin
        reset_done <= 1;
        clock_generator_configurator_reset <= 1;
        reset <= 1;
    end else begin
        clock_generator_configurator_reset <= 0;

        if (clock_manager_ready) begin
            reset <= 0;
        end
    end
end

wire clock_generator_ready;
wire clock_generator_error;

clock_generator_configurator #(
    .CLOCK_FREQ(`CONFIG_CLOCK_FREQ)
) clock_generator_configurator_inst(
    configuration_clock,
    clock_generator_configurator_reset,

    clock_generator_ready,
    clock_generator_error,

    clock_resetn,
    clock_scl,
    clock_sda
);

wire clock_manager_reset;
assign clock_manager_reset = !clock_generator_ready;

wire clock_manager_ready;

wire logic_clock;
wire mipi_dsi_clock;
wire mipi_csi_clock;
wire usb_transceiver_user_clock;
wire usb_transceiver_user_clock_2;

clock_manager #(
    .CLOCK_FREQ(`EXTERNAL_CLOCK_FREQ)
) clock_manager_inst(
    external_clock_p,
    external_clock_n,
    clock_manager_reset,

    clock_manager_ready,

    logic_clock,
    mipi_dsi_clock,
    mipi_csi_clock,
    usb_transceiver_user_clock,
    usb_transceiver_user_clock_2
);

wire [`FACE_TRACK_SCANLINE_SIZE - 1:0] eye_l_scanline;

video_input #(
    .LOGIC_CLOCK_FREQ(`LOGIC_CLOCK_FREQ),
    .MIPI_CSI_CLOCK_FREQ(`MIPI_CSI_CLOCK_FREQ),
    .SCANLINE_SIZE(`FACE_TRACK_SCANLINE_SIZE)
) video_input_eye_l(
    logic_clock,
    mipi_csi_clock,
    reset,

    eye_l_scanline,

    camera_eye_l_power_en,
    camera_eye_l_xclk,
    camera_eye_l_scl,
    camera_eye_l_sda,
    camera_eye_l_clock_p,
    camera_eye_l_clock_n,
    camera_eye_l_data_p,
    camera_eye_l_data_n
);

wire [`FACE_TRACK_SCANLINE_SIZE - 1:0] eye_r_scanline;

video_input #(
    .LOGIC_CLOCK_FREQ(`LOGIC_CLOCK_FREQ),
    .MIPI_CSI_CLOCK_FREQ(`MIPI_CSI_CLOCK_FREQ),
    .SCANLINE_SIZE(`FACE_TRACK_SCANLINE_SIZE)
) video_input_eye_r(
    logic_clock,
    mipi_csi_clock,
    reset,

    eye_r_scanline,

    camera_eye_r_power_en,
    camera_eye_r_xclk,
    camera_eye_r_scl,
    camera_eye_r_sda,
    camera_eye_r_clock_p,
    camera_eye_r_clock_n,
    camera_eye_r_data_p,
    camera_eye_r_data_n
);

wire [`FACE_TRACK_SCANLINE_SIZE - 1:0] mouth_scanline;

video_input #(
    .LOGIC_CLOCK_FREQ(`LOGIC_CLOCK_FREQ),
    .MIPI_CSI_CLOCK_FREQ(`MIPI_CSI_CLOCK_FREQ),
    .SCANLINE_SIZE(`FACE_TRACK_SCANLINE_SIZE)
) video_input_mouth(
    logic_clock,
    mipi_csi_clock,
    reset,

    mouth_scanline,

    camera_mouth_power_en,
    camera_mouth_xclk,
    camera_mouth_scl,
    camera_mouth_sda,
    camera_mouth_clock_p,
    camera_mouth_clock_n,
    camera_mouth_data_p,
    camera_mouth_data_n
);

wire [`PASSTHROUGH_SCANLINE_SIZE - 1:0] pass_l_scanline;

video_input #(
    .LOGIC_CLOCK_FREQ(`LOGIC_CLOCK_FREQ),
    .MIPI_CSI_CLOCK_FREQ(`MIPI_CSI_CLOCK_FREQ),
    .SCANLINE_SIZE(`PASSTHROUGH_SCANLINE_SIZE)
) video_input_pass_l(
    logic_clock,
    mipi_csi_clock,
    reset,

    pass_l_scanline,

    camera_pass_l_power_en,
    camera_pass_l_xclk,
    camera_pass_l_scl,
    camera_pass_l_sda,
    camera_pass_l_clock_p,
    camera_pass_l_clock_n,
    camera_pass_l_data_p,
    camera_pass_l_data_n
);

wire [`PASSTHROUGH_SCANLINE_SIZE - 1:0] pass_r_scanline;

video_input #(
    .LOGIC_CLOCK_FREQ(`LOGIC_CLOCK_FREQ),
    .MIPI_CSI_CLOCK_FREQ(`MIPI_CSI_CLOCK_FREQ),
    .SCANLINE_SIZE(`PASSTHROUGH_SCANLINE_SIZE)
) video_input_pass_r(
    logic_clock,
    mipi_csi_clock,
    reset,

    pass_r_scanline,

    camera_pass_r_power_en,
    camera_pass_r_xclk,
    camera_pass_r_scl,
    camera_pass_r_sda,
    camera_pass_r_clock_p,
    camera_pass_r_clock_n,
    camera_pass_r_data_p,
    camera_pass_r_data_n
);

wire video_enabled;

wire [`SCREEN_SCANLINE_SIZE - 1:0] screen_l_scanline;
wire video_l_ready;
wire video_l_error;

video_output #(
    .LOGIC_CLOCK_FREQ(`LOGIC_CLOCK_FREQ),
    .MIPI_DSI_CLOCK_FREQ(`MIPI_DSI_CLOCK_FREQ),
    .SCANLINE_SIZE(`SCREEN_SCANLINE_SIZE)
) video_output_l(
    logic_clock,
    mipi_dsi_clock,
    reset,

    screen_l_scanline,
    video_enabled,
    video_l_ready,
    video_l_error,

    screen_l_resetn,
    screen_l_aux,
    screen_l_clock_a_p,
    screen_l_clock_a_n,
    screen_l_data_a_p,
    screen_l_data_a_n,
    screen_l_clock_b_p,
    screen_l_clock_b_n,
    screen_l_data_b_p,
    screen_l_data_b_n
);

wire [`SCREEN_SCANLINE_SIZE - 1:0] screen_r_scanline;
wire video_r_ready;
wire video_r_error;

video_output #(
    .LOGIC_CLOCK_FREQ(`LOGIC_CLOCK_FREQ),
    .MIPI_DSI_CLOCK_FREQ(`MIPI_DSI_CLOCK_FREQ),
    .SCANLINE_SIZE(`SCREEN_SCANLINE_SIZE)
) video_output_r(
    logic_clock,
    mipi_dsi_clock,
    reset,

    screen_r_scanline,
    video_enabled,
    video_r_ready,
    video_r_error,

    screen_r_resetn,
    screen_r_aux,
    screen_r_clock_a_p,
    screen_r_clock_a_n,
    screen_r_data_a_p,
    screen_r_data_a_n,
    screen_r_clock_b_p,
    screen_r_clock_b_n,
    screen_r_data_b_p,
    screen_r_data_b_n
);

wire [15:0] base_station_tracking_envelope;
wire [15:0] base_station_tracking_data;
wire base_station_tracking_ready;

base_station_tracking_controller #(
    .CLOCK_FREQ(`LOGIC_CLOCK_FREQ)
) base_station_tracking_controller_inst(
    logic_clock,
    reset,

    base_station_tracking_envelope,
    base_station_tracking_data,
    base_station_tracking_ready,

    track_clock,
    track_data
);

wire [15:0] imu_data[5:0];
wire imu_error;

imu_controller #(
    .CLOCK_FREQ(`LOGIC_CLOCK_FREQ)
) imu_controller_inst(
    logic_clock,
    reset,

    imu_data,
    imu_error,

    imu_scl,
    imu_sda
);

wire [7:0] adjustment_eye_relief;
wire [7:0] adjustment_ipd;
wire adjustment_error;

adjustment_controller #(
    .CLOCK_FREQ(`LOGIC_CLOCK_FREQ)
) adjustment_controller_inst(
    logic_clock,
    reset,

    adjustment_eye_relief,
    adjustment_ipd,
    adjustment_error,

    adjust_scl,
    adjust_sda
);

wire in_packet_valid;
wire [`USB_MAX_PACKET_SIZE - 1:0] in_packet;
wire [`USB_PACKET_SIZE_SIZE - 1:0] in_packet_size;

wire out_packet_valid;
wire [`USB_MAX_PACKET_SIZE - 1:0] out_packet;
wire [`USB_PACKET_SIZE_SIZE - 1:0] out_packet_size;

usb_device #(
    .LOGIC_CLOCK_FREQ(`LOGIC_CLOCK_FREQ),
    .USB_USER_CLOCK_FREQ(`USB_USER_CLOCK_FREQ),
    .USB_USER_CLOCK_2_FREQ(`USB_USER_CLOCK_2_FREQ)
) usb_device_inst(
    logic_clock,
    usb_transceiver_user_clock,
    usb_transceiver_user_clock_2,
    reset,

    in_packet_valid,
    in_packet,
    in_packet_size,

    out_packet_valid,
    out_packet,
    out_packet_size,

    usb_pullup,
    usb_data_p,
    usb_data_n,

    usb_orientation,
    usb_open_transmit
);

usb_pd_placeholder usb_pd_placeholder_inst(
    usb_pd_scl,
    usb_pd_sda
);

wire [15:0] audio_in[`AUDIO_FRAME_SAMPLES * `AUDIO_CHANNELS - 1:0];
wire [15:0] audio_out[`AUDIO_FRAME_SAMPLES * `AUDIO_CHANNELS - 1:0];
wire audio_enabled;
wire audio_ready;
wire audio_error;

audio_controller #(
    .CLOCK_FREQ(`LOGIC_CLOCK_FREQ)
) audio_controller_inst(
    logic_clock,
    reset,

    audio_in,
    audio_out,
    audio_enabled,
    audio_ready,
    audio_error,

    audio_in_chip_select,
    audio_in_clock,
    audio_in_data_l,
    audio_in_data_r,

    audio_out_scl,
    audio_out_sda_l,
    audio_out_sda_r
);

wire video_ready;
wire video_error;

controller #(
    .CLOCK_FREQ(`LOGIC_CLOCK_FREQ)
) controller_inst(
    logic_clock,
    reset,

    in_packet_valid,
    in_packet,
    in_packet_size,

    out_packet_valid,
    out_packet,
    out_packet_size,

    screen_l_scanline,
    screen_r_scanline,
    video_enabled,
    video_ready,
    video_error,

    eye_l_scanline,
    eye_r_scanline,
    mouth_scanline,
    pass_l_scanline,
    pass_r_scanline,

    audio_in,
    audio_out,
    audio_enabled,
    audio_ready,
    audio_error,

    base_station_tracking_envelope,
    base_station_tracking_data,
    base_station_tracking_ready,

    adjustment_eye_relief,
    adjustment_ipd,
    adjustment_error,

    imu_data,
    imu_error
);

assign video_ready = video_l_ready & video_r_ready;
assign video_error = video_l_error | video_r_error;

endmodule

`endif
