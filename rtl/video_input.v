/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
`ifndef VIDEO_INPUT_V
`define VIDEO_INPUT_V

`include "constants.v"
`include "simulation/ibufds_dphy.v"
`include "mipi_hs_receiver.v"

module video_input(
    logic_clock,
    mipi_csi_clock,
    reset,

    scanline,

    power_en,
    xclk,
    scl,
    sda,
    mipi_clock_p,
    mipi_clock_n,
    mipi_data_p,
    mipi_data_n
);

input wire logic_clock;
input wire mipi_csi_clock;
input wire reset;

output reg [SCANLINE_SIZE - 1:0] scanline;

output reg power_en;
output reg xclk;
output reg scl;
inout wire sda;
input wire mipi_clock_p;
input wire mipi_clock_n;
inout wire [3:0] mipi_data_p;
inout wire [3:0] mipi_data_n;

parameter LOGIC_CLOCK_FREQ = 122400000;
parameter MIPI_CSI_CLOCK_FREQ = 36000000;
parameter SCANLINE_SIZE = 1920 * 3 * 8;

localparam HS_RX_BYTE_WIDTH = 8;

localparam [1:0] INIT_STAGE = 0;

reg sda_write;
reg sda_value;

reg high_speed;
reg [1:0] stage;

wire lp_clock_p;
wire lp_clock_n;
wire [3:0] lp_data_p;
wire [3:0] lp_data_n;

wire hs_clock;
wire [3:0] hs_data;

wire [7:0] hs_data_rx[HS_RX_BYTE_WIDTH - 1:0];

IBUFDS_DPHY clock_ibufds(
    .I(mipi_clock_p),
    .IB(mipi_clock_n),
    .HSRX_DISABLE(!high_speed),
    .HSRX_O(hs_clock),
    .LPRX_DISABLE(high_speed),
    .LPRX_O_N(lp_clock_n),
    .LPRX_O_P(lp_clock_p)
);

IBUFDS_DPHY data_ibufds[3:0](
    .I(mipi_data_p),
    .IB(mipi_data_n),
    .HSRX_DISABLE(!high_speed),
    .HSRX_O(hs_data),
    .LPRX_DISABLE(high_speed),
    .LPRX_O_N(lp_data_n),
    .LPRX_O_P(lp_data_p)
);

mipi_hs_receiver #(
    .BYTE_WIDTH(HS_RX_BYTE_WIDTH)
) receiver_inst(
    mipi_csi_clock,
    reset,

    high_speed,
    hs_data_rx,

    hs_clock,
    hs_data
);

assign sda = sda_write ? sda_value : 1'bz;

task do_reset; begin
    power_en <= 0;
    xclk <= 0;
    scl <= 1;

    sda_write <= 1;
    sda_value <= 1;

    high_speed <= 0;
    stage <= INIT_STAGE;
end endtask

task do_main; begin
    power_en <= 1;
    // TODO: CAMERA: Not implemented
end endtask

always @ (posedge logic_clock) begin
    if (reset) begin
        do_reset();
    end else begin
        do_main();
    end
end

endmodule

`endif
