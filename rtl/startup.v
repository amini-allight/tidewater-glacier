/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
/* Adapted from UG974 Vivado Ultrascale Libraries Guide */
`ifndef STARTUP_V
`define STARTUP_V

`include "constants.v"
`include "simulation/startupe3.v"

module startup(
    configuration_clock
);

output wire configuration_clock;
wire configuration_clock_oscillator;
wire [3:0] d_input;
wire end_of_startup;
wire preq;
wire [3:0] d_output;
wire [3:0] d_tristate;
wire fcs_output;
wire fcs_tristate;
wire global_reset;
wire global_tristate;
wire key_clear;
wire pack;
wire user_configuration_clock_input;
wire user_configuration_clock_tristate;
wire user_done_output;
wire user_done_tristate;

parameter CONFIG_CLOCK_FREQ = 100000000;

localparam SIM_CCLK_FREQ = `NANOSECONDS_PER_SECOND / $itor(CONFIG_CLOCK_FREQ);

// STARTUPE3: STARTUP Block
//            UltraScale
// Xilinx HDL Language Template, version 2024.1

STARTUPE3 #(
    .PROG_USR("FALSE"),                             // Activate program event security feature. Requires encrypted bitstreams.
    .SIM_CCLK_FREQ(SIM_CCLK_FREQ)                   // Set the Configuration Clock Frequency (ns) for simulation.
) STARTUPE3_inst (
    .CFGCLK(configuration_clock),                   // 1-bit output: Configuration main clock output.
    .CFGMCLK(configuration_clock_oscillator),       // 1-bit output: Configuration internal oscillator clock output.
    .DI(d_input),                                   // 4-bit output: Allow receiving on the D input pin.
    .EOS(end_of_startup),                           // 1-bit output: Active-High output signal indicating the End Of Startup.
    .PREQ(preq),                                    // 1-bit output: PROGRAM request to fabric output.
    .DO(d_output),                                  // 4-bit input: Allows control of the D pin output.
    .DTS(d_tristate),                               // 4-bit input: Allows tristate of the D pin.
    .FCSBO(fcs_output),                             // 1-bit input: Controls the FCS_B pin for flash access.
    .FCSBTS(fcs_tristate),                          // 1-bit input: Tristate the FCS_B pin.
    .GSR(global_reset),                             // 1-bit input: Global Set/Reset input (GSR cannot be used for the port).
    .GTS(global_tristate),                          // 1-bit input: Global 3-state input (GTS cannot be used for the port name).
    .KEYCLEARB(key_clear),                          // 1-bit input: Clear AES Decrypter Key input from Battery-Backed RAM (BBRAM).
    .PACK(pack),                                    // 1-bit input: PROGRAM acknowledge input.
    .USRCCLKO(user_configuration_clock_input),      // 1-bit input: User CCLK input.
    .USRCCLKTS(user_configuration_clock_tristate),  // 1-bit input: User CCLK 3-state enable input.
    .USRDONEO(user_done_output),                    // 1-bit input: User DONE pin output control.
    .USRDONETS(user_done_tristate)                  // 1-bit input: User DONE 3-state enable output.
);

// End of STARTUPE3_inst instantiation
endmodule

`endif
