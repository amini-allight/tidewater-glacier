/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
`ifndef MIPI_CRC_DECODER_V
`define MIPI_CRC_DECODER_V

// Receives data for N clocks while data_valid is high then takes one clock
// with data_valid low to check the finished checksum, error state is available
// on the next clock
module mipi_crc_decoder(
    clock,
    reset,

    data_valid,
    data,
    checksum,

    error
);

input wire clock;
input wire reset;

input wire data_valid;
input wire [7:0] data;
input wire [15:0] checksum;

output reg error;

reg [15:0] expected_checksum;

`include "common/mipi_crc_encoding.v"

task do_reset; begin
    error <= 0;

    expected_checksum <= 16'hffff;
end endtask

task do_main; begin
    if (data_valid) begin
        error <= 0;
        expected_checksum <= generate_crc(expected_checksum, data);
    end else begin
        error <= expected_checksum != checksum;
        expected_checksum <= 16'hffff;
    end
end endtask

always @ (posedge clock) begin
    if (reset) begin
        do_reset();
    end else begin
        do_main();
    end
end

endmodule

`endif
