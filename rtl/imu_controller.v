/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
`ifndef IMU_CONTROLLER_V
`define IMU_CONTROLLER_V

`include "constants.v"
`include "imu_reader.v"

module imu_controller(
    clock,
    reset,

    data,
    error,

    scl,
    sda
);

input wire clock;
input wire reset;

output reg [`IMU_WORD_SIZE - 1:0] data[FINAL_VALUE_INDEX:0];
output wire error;

output wire scl;
inout wire sda;

parameter CLOCK_FREQ = 122400000;

localparam FINAL_VALUE_INDEX = 5;
localparam [16:0] CLOCK_DIVISION = 17'(CLOCK_FREQ / `IMU_SAMPLE_FREQ);

integer i;

reg sample;
reg [16:0] clock_counter;

// Double buffered so the upstream controller always sees a valid value
wire [15:0] working_data[FINAL_VALUE_INDEX:0];

imu_reader #(
    .CLOCK_FREQ(CLOCK_FREQ)
) imu_reader_inst(
    clock,
    reset,

    sample,
    working_data,
    error,

    scl,
    sda
);

task do_reset; begin
    sample <= 0;
    for (i = 0; i <= FINAL_VALUE_INDEX; i = i + 1) begin
        data[i] <= 0;
    end
    clock_counter <= 0;
end endtask

task do_main; begin
    if (clock_counter == CLOCK_DIVISION) begin
        sample <= 1;
        for (i = 0; i <= FINAL_VALUE_INDEX; i = i + 1) begin
            data[i] <= working_data[i];
        end

        clock_counter <= 0;
    end else begin
        clock_counter <= clock_counter + 1;
    end

    if (sample) begin
        sample <= 0;
    end
end endtask

always @ (posedge clock) begin
    if (reset) begin
        do_reset();
    end else begin
        do_main();
    end
end

endmodule

`endif
