/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
`ifndef USB_PD_PLACEHOLDER_V
`define USB_PD_PLACEHOLDER_V

// These pins are only exposed for emergency use so this module does nothing
// except hold the i2c bus in its default state
module usb_pd_placeholder(
    scl,
    sda
);

output wire scl;
output wire sda;

assign scl = 1;
assign sda = 1;

endmodule

`endif
