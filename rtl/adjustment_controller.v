/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
`ifndef ADJUSTMENT_CONTROLLER_V
`define ADJUSTMENT_CONTROLLER_V

`include "constants.v"
`include "adjustment_reader.v"

module adjustment_controller(
    clock,
    reset,

    eye_relief,
    ipd,
    error,

    scl,
    sda
);

input wire clock;
input wire reset;

output reg [7:0] eye_relief;
output reg [7:0] ipd;
output wire error;

output wire scl;
inout wire sda;

parameter CLOCK_FREQ = 122400000;

localparam [20:0] CLOCK_DIVISION = 21'(CLOCK_FREQ / `ADJUSTMENT_SAMPLE_FREQ);

reg sample;
reg [20:0] clock_counter;

// Double buffered so the upstream controller always sees a valid value
wire [7:0] working_eye_relief;
wire [7:0] working_ipd;

adjustment_reader #(
    .CLOCK_FREQ(CLOCK_FREQ)
) adjustment_reader_inst(
    clock,
    reset,

    sample,
    working_eye_relief,
    working_ipd,
    error,

    scl,
    sda
);

task do_reset; begin
    eye_relief <= 0;
    ipd <= 0;

    sample <= 0;
    clock_counter <= 0;
end endtask

task do_main; begin
    if (clock_counter == CLOCK_DIVISION) begin
        sample <= 1;
        eye_relief <= working_eye_relief;
        ipd <= working_ipd;

        clock_counter <= 0;
    end else begin
        clock_counter <= clock_counter + 1;
    end

    if (sample) begin
        sample <= 0;
    end
end endtask

always @ (posedge clock) begin
    if (reset) begin
        do_reset();
    end else begin
        do_main();
    end
end

endmodule

`endif
