/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
`ifndef USB_DEVICE_V
`define USB_DEVICE_V

`include "constants.v"

module usb_device(
    logic_clock,
    usb_user_clock,
    usb_user_clock_2,
    reset,

    in_packet_valid,
    in_packet,
    in_packet_size,

    out_packet_valid,
    out_packet,
    out_packet_size,

    pullup_en,
    data_p,
    data_n,

    orientation,
    open_transmit
);

input wire logic_clock;
input wire usb_user_clock;
input wire usb_user_clock_2;
input wire reset;

output reg in_packet_valid;
output reg [`USB_MAX_PACKET_SIZE - 1:0] in_packet;
output reg [`USB_PACKET_SIZE_SIZE - 1:0] in_packet_size;

input wire out_packet_valid;
input wire [`USB_MAX_PACKET_SIZE - 1:0] out_packet;
input wire [`USB_PACKET_SIZE_SIZE - 1:0] out_packet_size;

output wire pullup_en;
inout wire data_p;
inout wire data_n;

input wire orientation;
output reg open_transmit;

parameter LOGIC_CLOCK_FREQ = 122400000;
parameter USB_USER_CLOCK_FREQ = 250000000;
parameter USB_USER_CLOCK_2_FREQ = 125000000;

task do_reset; begin
    in_packet_valid <= 0;
    in_packet <= 0;
    in_packet_size <= 0;

    open_transmit <= 1;
end endtask

task do_input; begin
    // TODO: USB: Not implemented
end endtask

task do_output; begin
    if (out_packet_valid) begin
        // TODO: USB: Not implemented
    end
end endtask

task do_main; begin
    do_input();

    do_output();
end endtask

always @ (posedge logic_clock) begin
    if (reset) begin
        do_reset();
    end else begin
        do_main();
    end
end

endmodule

`endif
