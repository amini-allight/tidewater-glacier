/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
`ifndef IMU_READER_V
`define IMU_READER_V

`include "constants.v"

module imu_reader(
    clock,
    reset,

    data_requested,
    data,
    error,

    scl,
    sda
);

input wire clock;
input wire reset;

input wire data_requested;
output reg [`IMU_WORD_SIZE - 1:0] data[5:0];
output reg error;

output reg scl;
inout wire sda;

parameter CLOCK_FREQ = 122400000;

localparam [3:0] IDLE_STAGE = 0;
localparam [3:0] START_STAGE = 1;
localparam [3:0] ADDRESS_STAGE = 2;
localparam [3:0] ADDRESS_ACK_STAGE = 3;
localparam [3:0] WRITE_STAGE = 4;
localparam [3:0] WRITE_ACK_STAGE = 5;
localparam [3:0] READ_STAGE = 6;
localparam [3:0] READ_ACK_STAGE = 7;
localparam [3:0] STOP_STAGE = 8;

localparam [2:0] FINAL_BIT_INDEX = 7;

localparam [8:0] CLOCK_DIVISION = 9'(CLOCK_FREQ
    / `I2C_FAST_CLOCK_FREQ / 2);

localparam [0:6] IMU_ADDRESS = 7'b1101010;

localparam [0:7] REGISTER_ADDRESSES[0:11] = {
    8'h23, 8'h22,
    8'h25, 8'h24,
    8'h27, 8'h26,
    8'h29, 8'h28,
    8'h2b, 8'h2a,
    8'h2d, 8'h2c
};

localparam [3:0] FINAL_REGISTER_INDEX = 11;

reg [3:0] stage;
reg [2:0] sub_stage;
reg go_next;
reg [3:0] next_stage;

reg [3:0] register_index;

reg write;
reg write_value;

reg [8:0] clock_counter;

assign sda = write_value ? write : 1'bz;

task do_reset; begin
    error <= 0;

    scl <= 1;

    stage <= IDLE_STAGE;
    sub_stage <= 0;
    go_next <= 0;
    next_stage <= WRITE_STAGE;

    register_index <= 0;

    write <= 1;
    write_value <= 1;

    clock_counter <= 0;
end endtask

task do_start; begin
    if (scl) begin
        write_value <= 0;
        stage <= ADDRESS_STAGE;
    end else begin
        scl <= 1;
        write_value <= 1;
    end
end endtask

task do_address; begin
    if (!scl) begin
        if (sub_stage != 7) begin
            write_value <= IMU_ADDRESS[sub_stage];
        end else begin
            write_value <= next_stage == READ_STAGE;
        end
        sub_stage <= sub_stage + 1;
        go_next <= (sub_stage == FINAL_BIT_INDEX);
    end else begin
        if (go_next) begin
            go_next <= 0;
            stage <= ADDRESS_ACK_STAGE;
        end
    end

    scl <= !scl;
end endtask

task do_address_ack; begin
    if (!scl) begin
        write <= 0;
        write_value <= 0;
    end else begin
        write <= 1;

        if (sda) begin
            error <= 1;
            stage <= STOP_STAGE;
            next_stage <= WRITE_STAGE;
        end else begin
            stage <= next_stage;
        end
    end

    scl <= !scl;
end endtask

task do_write; begin
    if (!scl) begin
        write_value <= REGISTER_ADDRESSES[register_index][sub_stage];
        sub_stage <= sub_stage + 1;
        go_next <= (sub_stage == FINAL_BIT_INDEX);
    end else begin
        if (go_next) begin
            go_next <= 0;
            stage <= WRITE_ACK_STAGE;
        end
    end

    scl <= !scl;
end endtask

task do_write_ack; begin
    if (!scl) begin
        write <= 0;
        write_value <= 0;
    end else begin
        write <= 1;

        if (sda) begin
            error <= 1;
            stage <= STOP_STAGE;
            next_stage <= WRITE_STAGE;
        end else begin
            stage <= START_STAGE;
            next_stage <= READ_STAGE;
        end
    end

    scl <= !scl;
end endtask

task do_read; begin
     if (!scl) begin
        write <= 0;
    end else begin
        data[register_index / 2][(`IMU_WORD_SIZE - 1) - (((32'(register_index) % 2) * 8) + 32'(sub_stage))] <= sda;
        sub_stage <= sub_stage + 1;
        if (sub_stage == FINAL_BIT_INDEX) begin
            stage <= READ_ACK_STAGE;
        end
    end

    scl <= !scl;
end endtask

task do_read_ack; begin
    if (!scl) begin
        write <= 1;
        write_value <= 0;
    end else begin
        stage <= STOP_STAGE;
        next_stage <= WRITE_STAGE;
    end

    scl <= !scl;
end endtask

task do_stop; begin
    if (scl) begin
        write_value <= 1;
        if (register_index != FINAL_REGISTER_INDEX) begin
            stage <= START_STAGE;
            register_index <= register_index + 1;
        end else begin
            stage <= IDLE_STAGE;
            register_index <= 0;
        end
    end else begin
        scl <= 1;
        write_value <= 0;
    end
end endtask

task do_transmit; begin
    if (clock_counter == CLOCK_DIVISION) begin
        case (stage)
        default: begin end
        START_STAGE: do_start();
        ADDRESS_STAGE: do_address();
        ADDRESS_ACK_STAGE: do_address_ack();
        WRITE_STAGE: do_write();
        WRITE_ACK_STAGE: do_write_ack();
        READ_STAGE: do_read();
        READ_ACK_STAGE: do_read_ack();
        STOP_STAGE: do_stop();
        endcase

        clock_counter <= 0;
    end else begin
        clock_counter <= clock_counter + 1;
    end
end endtask

task do_main; begin
    if (data_requested) begin
        if (stage == IDLE_STAGE) begin
            stage <= START_STAGE;
        end
    end else begin
        do_transmit();
    end
end endtask

always @ (posedge clock) begin
    if (reset) begin
        do_reset();
    end else begin
        do_main();
    end
end

endmodule

`endif
