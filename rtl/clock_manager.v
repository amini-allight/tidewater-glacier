/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
/* Adapted from UG974 Vivado Ultrascale Libraries Guide */
`ifndef CLOCK_MANAGER_V
`define CLOCK_MANAGER_V

`include "constants.v"
`include "simulation/ibufds.v"
`include "simulation/mmcme4_base.v"
`include "simulation/plle4_base.v"

module clock_manager(
    external_clock_p,
    external_clock_n,
    reset,

    ready,

    logic_clock,
    mipi_dsi_clock,
    mipi_csi_clock,
    usb_transceiver_user_clock,
    usb_transceiver_user_clock_2
);

input wire external_clock_p;
input wire external_clock_n;
input wire reset;

output wire ready;

output wire logic_clock;
output wire mipi_dsi_clock;
output wire mipi_csi_clock;
output wire usb_transceiver_user_clock;
output wire usb_transceiver_user_clock_2;

parameter CLOCK_FREQ = 500000000;

localparam CLOCK_PERIOD = `NANOSECONDS_PER_SECOND / $itor(CLOCK_FREQ);

wire external_clock;

IBUFDS IBUFDS_inst(
    .I(external_clock_p),
    .IB(external_clock_n),
    .O(external_clock)
);

wire mmcm_feedback;
wire mmcm_feedback_inverted;

wire mmcm_clock_0;
wire mmcm_clock_0_inverted;
wire mmcm_clock_1;
wire mmcm_clock_1_inverted;
wire mmcm_clock_2;
wire mmcm_clock_2_inverted;
wire mmcm_clock_3;
wire mmcm_clock_3_inverted;
wire mmcm_clock_4;
wire mmcm_clock_5;
wire mmcm_clock_6;

wire mmcm_locked;

MMCME4_BASE #(
   .BANDWIDTH("OPTIMIZED"),         // Jitter programming
   .CLKFBOUT_MULT_F(2.448),         // Multiply value for all CLKOUT
   .CLKFBOUT_PHASE(0.0),            // Phase offset in degrees of CLKFB
   .CLKIN1_PERIOD(CLOCK_PERIOD),    // Input clock period in ns to ps resolution (i.e., 33.333 is 30 MHz).
   .CLKOUT0_DIVIDE_F(1.0),          // Divide amount for CLKOUT0
   .CLKOUT0_DUTY_CYCLE(0.5),        // Duty cycle for CLKOUT0
   .CLKOUT0_PHASE(0.0),             // Phase offset for CLKOUT0
   .CLKOUT1_DIVIDE(10),             // Divide amount for CLKOUT (1-128)
   .CLKOUT1_DUTY_CYCLE(0.5),        // Duty cycle for CLKOUT outputs (0.001-0.999).
   .CLKOUT1_PHASE(0.0),             // Phase offset for CLKOUT outputs (-360.000-360.000).
   .CLKOUT2_DIVIDE(1),              // Divide amount for CLKOUT (1-128)
   .CLKOUT2_DUTY_CYCLE(0.5),        // Duty cycle for CLKOUT outputs (0.001-0.999).
   .CLKOUT2_PHASE(0.0),             // Phase offset for CLKOUT outputs (-360.000-360.000).
   .CLKOUT3_DIVIDE(1),              // Divide amount for CLKOUT (1-128)
   .CLKOUT3_DUTY_CYCLE(0.5),        // Duty cycle for CLKOUT outputs (0.001-0.999).
   .CLKOUT3_PHASE(0.0),             // Phase offset for CLKOUT outputs (-360.000-360.000).
   .CLKOUT4_CASCADE("TRUE"),        // Cascade the CLKOUT4 and CLKOUT6 dividers together
   .CLKOUT4_DIVIDE(4),              // Divide amount for CLKOUT (1-128)
   .CLKOUT4_DUTY_CYCLE(0.5),        // Duty cycle for CLKOUT outputs (0.001-0.999).
   .CLKOUT4_PHASE(0.0),             // Phase offset for CLKOUT outputs (-360.000-360.000).
   .CLKOUT5_DIVIDE(1),              // Divide amount for CLKOUT (1-128)
   .CLKOUT5_DUTY_CYCLE(0.5),        // Duty cycle for CLKOUT outputs (0.001-0.999).
   .CLKOUT5_PHASE(0.0),             // Phase offset for CLKOUT outputs (-360.000-360.000).
   .CLKOUT6_DIVIDE(85),             // Divide amount for CLKOUT (1-128)
   .CLKOUT6_DUTY_CYCLE(0.5),        // Duty cycle for CLKOUT outputs (0.001-0.999).
   .CLKOUT6_PHASE(0.0),             // Phase offset for CLKOUT outputs (-360.000-360.000).
   .DIVCLK_DIVIDE(1),               // Master division value
   .IS_CLKFBIN_INVERTED(1'b0),      // Optional inversion for CLKFBIN
   .IS_CLKIN1_INVERTED(1'b0),       // Optional inversion for CLKIN1
   .IS_PWRDWN_INVERTED(1'b0),       // Optional inversion for PWRDWN
   .IS_RST_INVERTED(1'b0),          // Optional inversion for RST
   .REF_JITTER1(0.0),               // Reference input jitter in UI (0.000-0.999).
   .STARTUP_WAIT("FALSE")           // Delays DONE until MMCM is locked
) MMCM_inst(
   .CLKFBOUT(mmcm_feedback),                // 1-bit output: Feedback clock pin to the MMCM
   .CLKFBOUTB(mmcm_feedback_inverted),      // 1-bit output: Inverted CLKFBOUT
   .CLKOUT0(mmcm_clock_0),                  // 1-bit output: CLKOUT0
   .CLKOUT0B(mmcm_clock_0_inverted),        // 1-bit output: Inverted CLKOUT0
   .CLKOUT1(mmcm_clock_1),                  // 1-bit output: CLKOUT1
   .CLKOUT1B(mmcm_clock_1_inverted),        // 1-bit output: Inverted CLKOUT1
   .CLKOUT2(mmcm_clock_2),                  // 1-bit output: CLKOUT2
   .CLKOUT2B(mmcm_clock_2_inverted),        // 1-bit output: Inverted CLKOUT2
   .CLKOUT3(mmcm_clock_3),                  // 1-bit output: CLKOUT3
   .CLKOUT3B(mmcm_clock_3_inverted),        // 1-bit output: Inverted CLKOUT3
   .CLKOUT4(mmcm_clock_4),                  // 1-bit output: CLKOUT4
   .CLKOUT5(mmcm_clock_5),                  // 1-bit output: CLKOUT5
   .CLKOUT6(mmcm_clock_6),                  // 1-bit output: CLKOUT6
   .LOCKED(mmcm_locked),                    // 1-bit output: LOCK
   .CLKFBIN(mmcm_feedback),                 // 1-bit input: Feedback clock pin to the MMCM
   .CLKIN1(external_clock),                 // 1-bit input: Primary clock
   .PWRDWN(1'b0),                           // 1-bit input: Power-down
   .RST(reset)                              // 1-bit input: Reset
);

wire pll_feedback;

wire pll_clock_phy;

wire pll_clock_0;
wire pll_clock_0_inverted;
wire pll_clock_1;
wire pll_clock_1_inverted;

wire pll_locked;

PLLE4_BASE #(
   .CLKFBOUT_MULT(2),          // Multiply value for all CLKOUT
   .CLKFBOUT_PHASE(0.0),       // Phase offset in degrees of CLKFB
   .CLKIN_PERIOD(0.0),         // Input clock period in ns to ps resolution (i.e., 33.333 is 30 MHz).
   .CLKOUT0_DIVIDE(4),         // Divide amount for CLKOUT0
   .CLKOUT0_DUTY_CYCLE(0.5),   // Duty cycle for CLKOUT0
   .CLKOUT0_PHASE(0.0),        // Phase offset for CLKOUT0
   .CLKOUT1_DIVIDE(8),         // Divide amount for CLKOUT1
   .CLKOUT1_DUTY_CYCLE(0.5),   // Duty cycle for CLKOUT1
   .CLKOUT1_PHASE(0.0),        // Phase offset for CLKOUT1
   .CLKOUTPHY_MODE("VCO_2X"),  // Frequency of the CLKOUTPHY
   .DIVCLK_DIVIDE(1),          // Master division value
   .IS_CLKFBIN_INVERTED(1'b0), // Optional inversion for CLKFBIN
   .IS_CLKIN_INVERTED(1'b0),   // Optional inversion for CLKIN
   .IS_PWRDWN_INVERTED(1'b0),  // Optional inversion for PWRDWN
   .IS_RST_INVERTED(1'b0),     // Optional inversion for RST
   .REF_JITTER(0.0),           // Reference input jitter in UI
   .STARTUP_WAIT("FALSE")      // Delays DONE until PLL is locked
) PLLE4_BASE_inst(
   .CLKFBOUT(pll_feedback),             // 1-bit output: Feedback clock
   .CLKOUT0(pll_clock_0),               // 1-bit output: General Clock output
   .CLKOUT0B(pll_clock_0_inverted),     // 1-bit output: Inverted CLKOUT0
   .CLKOUT1(pll_clock_1),               // 1-bit output: General Clock output
   .CLKOUT1B(pll_clock_1_inverted),     // 1-bit output: Inverted CLKOUT1
   .CLKOUTPHY(pll_clock_phy),           // 1-bit output: Bitslice clock
   .LOCKED(pll_locked),                 // 1-bit output: LOCK
   .CLKFBIN(pll_feedback),              // 1-bit input: Feedback clock
   .CLKIN(external_clock),              // 1-bit input: Input clock
   .CLKOUTPHYEN(1'b0),                  // 1-bit input: CLKOUTPHY enable
   .PWRDWN(1'b0),                       // 1-bit input: Power-down
   .RST(reset)                          // 1-bit input: Reset
);

assign ready = mmcm_locked && pll_locked;
// 500 MHz
assign mipi_dsi_clock = external_clock;
// 122.4 MHz
assign logic_clock = mmcm_clock_1;
// 36 MHz
assign mipi_csi_clock = mmcm_clock_4;
// 250 MHz
assign usb_transceiver_user_clock = pll_clock_0;
// 125 MHz
assign usb_transceiver_user_clock_2 = pll_clock_1;

endmodule

`endif
