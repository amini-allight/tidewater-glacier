/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
`ifndef MIPI_ECC_DECODER_V
`define MIPI_ECC_DECODER_V

`include "mipi_ecc_encoder.v"

// Accepts an input every clock, emits a result the next clock
module mipi_ecc_decoder(
    clock,
    reset,

    header_in,
    ecc_in,

    header_out,
    error
);

input wire clock;
input wire reset;

input wire [23:0] header_in;
input wire [7:0] ecc_in;

output reg [23:0] header_out;
output reg error;

`include "common/mipi_ecc_encoding.v"

task do_reset; begin
    header_out <= 0;
    error <= 0;
end endtask

task do_main; begin
    if (calculate_ecc(header_in) != ecc_in) begin
        case (calculate_ecc(header_in) ^ ecc_in)
        default: begin
            header_out <= header_in;
            error <= 1;
        end
        8'h07: begin
            header_out <= header_in ^ (24'b1 << 0);
            error <= 0;
        end
        8'h0b: begin
            header_out <= header_in ^ (24'b1 << 1);
            error <= 0;
        end
        8'h0d: begin
            header_out <= header_in ^ (24'b1 << 2);
            error <= 0;
        end
        8'h0e: begin
            header_out <= header_in ^ (24'b1 << 3);
            error <= 0;
        end
        8'h13: begin
            header_out <= header_in ^ (24'b1 << 4);
            error <= 0;
        end
        8'h15: begin
            header_out <= header_in ^ (24'b1 << 5);
            error <= 0;
        end
        8'h16: begin
            header_out <= header_in ^ (24'b1 << 6);
            error <= 0;
        end
        8'h19: begin
            header_out <= header_in ^ (24'b1 << 7);
            error <= 0;
        end
        8'h1a: begin
            header_out <= header_in ^ (24'b1 << 8);
            error <= 0;
        end
        8'h1c: begin
            header_out <= header_in ^ (24'b1 << 9);
            error <= 0;
        end
        8'h23: begin
            header_out <= header_in ^ (24'b1 << 10);
            error <= 0;
        end
        8'h25: begin
            header_out <= header_in ^ (24'b1 << 11);
            error <= 0;
        end
        8'h26: begin
            header_out <= header_in ^ (24'b1 << 12);
            error <= 0;
        end
        8'h29: begin
            header_out <= header_in ^ (24'b1 << 13);
            error <= 0;
        end
        8'h2a: begin
            header_out <= header_in ^ (24'b1 << 14);
            error <= 0;
        end
        8'h2c: begin
            header_out <= header_in ^ (24'b1 << 15);
            error <= 0;
        end
        8'h31: begin
            header_out <= header_in ^ (24'b1 << 16);
            error <= 0;
        end
        8'h32: begin
            header_out <= header_in ^ (24'b1 << 17);
            error <= 0;
        end
        8'h34: begin
            header_out <= header_in ^ (24'b1 << 18);
            error <= 0;
        end
        8'h38: begin
            header_out <= header_in ^ (24'b1 << 19);
            error <= 0;
        end
        8'h1f: begin
            header_out <= header_in ^ (24'b1 << 20);
            error <= 0;
        end
        8'h2f: begin
            header_out <= header_in ^ (24'b1 << 21);
            error <= 0;
        end
        8'h37: begin
            header_out <= header_in ^ (24'b1 << 22);
            error <= 0;
        end
        8'h3b: begin
            header_out <= header_in ^ (24'b1 << 23);
            error <= 0;
        end
        endcase
    end else begin
        header_out <= header_in;
        error <= 0;
    end
end endtask

always @ (posedge clock) begin
    if (reset) begin
        do_reset();
    end else begin
        do_main();
    end
end

endmodule

`endif
