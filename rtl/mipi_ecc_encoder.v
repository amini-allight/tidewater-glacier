/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
`ifndef MIPI_ECC_ENCODER_V
`define MIPI_ECC_ENCODER_V

// Accepts an input every clock, emits a result the next clock
module mipi_ecc_encoder(
    clock,
    reset,

    header,

    ecc
);

input wire clock;
input wire reset;

input wire [23:0] header;

output reg [7:0] ecc;

`include "common/mipi_ecc_encoding.v"

task do_reset; begin
    ecc <= 0;
end endtask

task do_main; begin
    ecc <= calculate_ecc(header);
end endtask

always @ (posedge clock) begin
    if (reset) begin
        do_reset();
    end else begin
        do_main();
    end
end

endmodule

`endif
