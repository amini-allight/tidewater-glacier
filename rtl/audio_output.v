/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
`ifndef AUDIO_OUTPUT_V
`define AUDIO_OUTPUT_V

`include "constants.v"

// over the 18 clocks (@ 3.4 MHz) following a data_ready toggle (excluding the
// clock that occurred on) this will write out. At some point after that 18th
// clock data_ready must be toggled again to send the next sample to the DAC
module audio_output(
    clock,
    reset,
    data,
    data_ready,
    ready,
    error,

    scl,
    sda_l,
    sda_r
);

input wire clock;
input wire reset;

input wire [`AUDIO_BIT_DEPTH - 1:0] data[`AUDIO_CHANNELS - 1:0];
input wire data_ready;
output reg ready;
output reg error;

output reg scl;
inout wire sda_l;
inout wire sda_r;

parameter CLOCK_FREQ = 122400000;

localparam [1:0] START_INIT_STAGE = 0;
localparam [1:0] HIGH_SPEED_INIT_STAGE = 1;
localparam [1:0] END_INIT_STAGE = 2;

localparam [2:0] IDLE_STAGE = 0;
localparam [2:0] START_STAGE = 1;
localparam [2:0] ADDRESS_STAGE = 2;
localparam [2:0] ADDRESS_ACK_STAGE = 3;
localparam [2:0] WRITE_STAGE = 4;
localparam [2:0] WRITE_ACK_STAGE = 5;
localparam [2:0] STOP_STAGE = 6;

localparam [0:7] HIGH_SPEED_CODE = 8'b00001000;

localparam [2:0] FINAL_BIT_INDEX = 7;

localparam [8:0] FAST_CLOCK_DIVISION = 9'(CLOCK_FREQ
    / `I2C_FAST_CLOCK_FREQ / 2);
localparam [8:0] HIGH_SPEED_CLOCK_DIVISION = 9'(CLOCK_FREQ
    / `I2C_HIGH_SPEED_CLOCK_FREQ / 2);

// Has the write flag appended to the end
localparam [0:7] DAC_ADDRESS = 8'b10011000;

integer i;

reg write;
reg write_value_l;
reg write_value_r;

reg [1:0] init_stage;
reg [2:0] stage;
reg [2:0] sub_stage;
reg go_next;
reg byte_index;
reg data_available;

reg [8:0] clock_counter;

wire [0:`AUDIO_BIT_DEPTH - 1] data_big_endian[`AUDIO_CHANNELS - 1:0];

generate for (genvar i = 0; i < `AUDIO_BIT_DEPTH * `AUDIO_CHANNELS; i = i + 1) begin
    assign data_big_endian[i / `AUDIO_BIT_DEPTH][i % `AUDIO_BIT_DEPTH] = data[i / `AUDIO_BIT_DEPTH][i % `AUDIO_BIT_DEPTH];
end endgenerate

assign sda_l = write ? write_value_l : 1'bz;
assign sda_r = write ? write_value_r : 1'bz;

task do_reset; begin
    ready <= 0;
    error <= 0;

    scl <= 1;

    write <= 1;
    write_value_l <= 1;
    write_value_r <= 1;

    init_stage <= START_INIT_STAGE;
    stage <= IDLE_STAGE;
    sub_stage <= 0;
    go_next <= 0;
    byte_index <= 0;
    data_available <= 0;

    clock_counter <= 0;
end endtask

task do_start_init; begin
    write_value_l <= 0;
    write_value_r <= 0;
    init_stage <= HIGH_SPEED_INIT_STAGE;
end endtask

task do_high_speed_init; begin
    if (!scl) begin
        write_value_l <= HIGH_SPEED_CODE[sub_stage];
        write_value_r <= HIGH_SPEED_CODE[sub_stage];
        sub_stage <= sub_stage + 1;

        if (sub_stage == FINAL_BIT_INDEX) begin
            init_stage <= END_INIT_STAGE;
        end
    end

    scl <= !scl;
end endtask

task do_end_init; begin
    // We arrive here at scl = 1
    scl <= 0;
    ready <= 1;
end endtask

task do_init; begin
    if (clock_counter == FAST_CLOCK_DIVISION) begin
        case (init_stage)
        default: begin end
        START_INIT_STAGE: do_start_init();
        HIGH_SPEED_INIT_STAGE: do_high_speed_init();
        END_INIT_STAGE: do_end_init();
        endcase

        clock_counter <= 0;
    end else begin
        clock_counter <= clock_counter + 1;
    end
end endtask

task do_start; begin
    write_value_l <= 0;
    write_value_r <= 0;
    stage <= ADDRESS_STAGE;
end endtask

task do_address; begin
    if (!scl) begin
        write_value_l <= DAC_ADDRESS[sub_stage];
        write_value_r <= DAC_ADDRESS[sub_stage];
        sub_stage <= sub_stage + 1;
        go_next <= (sub_stage == FINAL_BIT_INDEX);
    end else begin
        if (go_next) begin
            go_next <= 0;
            stage <= ADDRESS_ACK_STAGE;
        end
    end

    scl <= !scl;
end endtask

task do_address_ack; begin
    // We arrive here at scl = 0
    if (!scl) begin
        write <= 0;
        write_value_l <= 0;
        write_value_r <= 0;
    end else begin
        write <= 1;

        if (sda_l || sda_r) begin
            error <= 1;
            stage <= STOP_STAGE;
        end else begin
            stage <= WRITE_STAGE;
        end
    end

    scl <= !scl;
end endtask

task do_write; begin
    if (data_available) begin
        if (!scl) begin
            write_value_l <= data_big_endian[0][(byte_index * 8) + sub_stage];
            write_value_r <= data_big_endian[1][(byte_index * 8) + sub_stage];
            sub_stage <= sub_stage + 1;
            go_next <= (sub_stage == FINAL_BIT_INDEX);
        end else begin
            if (go_next) begin
                go_next <= 0;
                if (byte_index == 1) begin
                    data_available <= 0;
                end
                byte_index <= byte_index + 1;
                stage <= WRITE_ACK_STAGE;
            end
        end

        scl <= !scl;
    end
end endtask

task do_write_ack; begin
    // We arrive here at scl = 0
    if (!scl) begin
        write <= 0;
        write_value_l <= 0;
        write_value_r <= 0;
    end else begin
        write <= 1;

        if (sda_l || sda_r) begin
            error <= 1;
            stage <= STOP_STAGE;
        end else begin
            stage <= WRITE_STAGE;
        end
    end

    scl <= !scl;
end endtask

task do_stop; begin
    // We arrive here at scl = 0
    if (scl) begin
        write_value_l <= 1;
        write_value_r <= 1;
        stage <= IDLE_STAGE;
    end else begin
        scl <= 1;
        write_value_l <= 0;
        write_value_r <= 0;
    end
end endtask

task do_transmit; begin
    if (clock_counter == HIGH_SPEED_CLOCK_DIVISION) begin
        case (stage)
        default: begin end
        START_STAGE: do_start();
        ADDRESS_STAGE: do_address();
        ADDRESS_ACK_STAGE: do_address_ack();
        WRITE_STAGE: do_write();
        WRITE_ACK_STAGE: do_write_ack();
        STOP_STAGE: do_stop();
        endcase

        clock_counter <= 0;
    end else begin
        clock_counter <= clock_counter + 1;
    end
end endtask

task do_main; begin
    if (!ready) begin
        do_init();
    end else begin
        if (data_ready) begin
            if (stage == IDLE_STAGE) begin
                stage <= ADDRESS_STAGE;
            end
            data_available <= 1;
        end else begin
            do_transmit();
        end
    end
end endtask

always @ (posedge clock) begin
    if (reset) begin
        do_reset();
    end else begin
        do_main();
    end
end

endmodule

`endif
