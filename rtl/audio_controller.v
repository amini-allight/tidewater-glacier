/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
`ifndef AUDIO_CONTROLLER_V
`define AUDIO_CONTROLLER_V

`include "constants.v"
`include "audio_input.v"
`include "audio_output.v"

// Input/output data blocks need to be filled/read at 2 kHz
module audio_controller(
    clock,
    reset,

    audio_in,
    audio_out,
    enabled,
    ready,
    error,

    audio_in_chip_select,
    audio_in_clock,
    audio_in_data_l,
    audio_in_data_r,

    audio_out_scl,
    audio_out_sda_l,
    audio_out_sda_r
);

input wire clock;
input wire reset;

output reg [`AUDIO_BIT_DEPTH - 1:0] audio_in[`AUDIO_FRAME_SAMPLES * `AUDIO_CHANNELS - 1:0];
input wire [`AUDIO_BIT_DEPTH - 1:0] audio_out[`AUDIO_FRAME_SAMPLES * `AUDIO_CHANNELS - 1:0];
input wire enabled;
output wire ready;
output wire error;

output wire audio_in_chip_select;
output wire audio_in_clock;
input wire audio_in_data_l;
input wire audio_in_data_r;

output wire audio_out_scl;
inout wire audio_out_sda_l;
inout wire audio_out_sda_r;

parameter CLOCK_FREQ = 122400000;

localparam [11:0] CLOCK_DIVISION = 12'(CLOCK_FREQ / `AUDIO_FREQ);
localparam INDEX_SIZE = $clog2(`AUDIO_FRAME_SAMPLES);
localparam [INDEX_SIZE - 1:0] FINAL_INDEX = `AUDIO_FRAME_SAMPLES - 1;

integer i;

reg sample;
reg [INDEX_SIZE - 1:0] index;
reg [11:0] clock_counter;

wire [`AUDIO_BIT_DEPTH - 1:0] in_data[`AUDIO_CHANNELS - 1:0];
wire in_error;

reg [`AUDIO_BIT_DEPTH - 1:0] out_data[`AUDIO_CHANNELS - 1:0];
wire out_error;

audio_input #(
    .CLOCK_FREQ(CLOCK_FREQ)
) audio_input_inst(
    clock,
    reset,

    sample,
    in_data,
    in_error,

    audio_in_chip_select,
    audio_in_clock,
    audio_in_data_l,
    audio_in_data_r
);

audio_output #(
    .CLOCK_FREQ(CLOCK_FREQ)
) audio_output_inst(
    clock,
    reset,

    out_data,
    sample,
    ready,
    out_error,

    audio_out_scl,
    audio_out_sda_l,
    audio_out_sda_r
);

assign error = in_error || out_error;

task do_reset; begin
    for (i = 0; i < `AUDIO_FRAME_SAMPLES * `AUDIO_CHANNELS; i = i + 1) begin
        audio_in[i] <= 0;
    end

    sample <= 0;
    index <= 0;
    clock_counter <= 0;

    for (i = 0; i < `AUDIO_CHANNELS; i = i + 1) begin
        out_data[i] <= 0;
    end
end endtask

task do_main; begin
    if (enabled) begin
        if (clock_counter == CLOCK_DIVISION) begin
            sample <= 1;
            out_data[0] <= audio_out[index * `AUDIO_CHANNELS + 0];
            out_data[1] <= audio_out[index * `AUDIO_CHANNELS + 1];
            audio_in[index * `AUDIO_CHANNELS + 0] <= in_data[0];
            audio_in[index * `AUDIO_CHANNELS + 1] <= in_data[1];
            if (index == FINAL_INDEX) begin
                index <= 0;
            end else begin
                index <= index + 1;
            end

            clock_counter <= 0;
        end else begin
            clock_counter <= clock_counter + 1;
        end

        if (sample) begin
            sample <= 0;
        end
    end
end endtask

always @ (posedge clock) begin
    if (reset) begin
        do_reset();
    end else begin
        do_main();
    end
end

endmodule

`endif
