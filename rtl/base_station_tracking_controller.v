/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
`ifndef BASE_STATION_TRACKING_CONTROLLER_V
`define BASE_STATION_TRACKING_CONTROLLER_V

`include "constants.v"

module base_station_tracking_controller(
    clock,
    reset,

    envelope,
    data,
    ready,

    track_clock,
    track_data
);

input wire clock;
input wire reset;

output reg [`TRACK_SENSOR_COUNT - 1:0] envelope;
output reg [`TRACK_SENSOR_COUNT - 1:0] data;
output reg ready;

inout wire [`TRACK_SENSOR_COUNT - 1:0] track_clock;
inout wire [`TRACK_SENSOR_COUNT - 1:0] track_data;

parameter CLOCK_FREQ = 122400000;

localparam [2:0] START_CONFIG_STAGE = 0;
localparam [2:0] START_WRITE_CONFIG_STAGE = 1;
localparam [2:0] WRITE_CONFIG_STAGE = 2;
localparam [2:0] END_WRITE_CONFIG_STAGE = 3;
localparam [2:0] END_CONFIG_STAGE = 4;
localparam [2:0] LISTEN_STAGE = 5;

localparam BUS_CLOCK_FREQ = 5100000;

localparam [`TRACK_SENSOR_COUNT - 1:0] ALL_HIGH = `TRACK_SENSOR_COUNT'b1111111111111111;
localparam [`TRACK_SENSOR_COUNT - 1:0] ALL_LOW = `TRACK_SENSOR_COUNT'b0000000000000000;

localparam [16:0] BUS_CLOCK_DIVISION = 17'(CLOCK_FREQ / BUS_CLOCK_FREQ / 2);
localparam [16:0] SAMPLE_CLOCK_DIVISION = 17'(CLOCK_FREQ / `BASE_STATION_TRACKING_SAMPLE_FREQ);

localparam CONFIG_LENGTH = 14;

integer i;

reg clock_write;
reg [`TRACK_SENSOR_COUNT - 1:0] clock_write_values;
reg data_write;
reg [`TRACK_SENSOR_COUNT - 1:0] data_write_values;

reg [2:0] stage;
reg [3:0] sub_stage;

reg [16:0] clock_counter;

reg [0:CONFIG_LENGTH - 1] config_data;

assign track_clock = clock_write ? clock_write_values : `TRACK_SENSOR_COUNT'bz;
assign track_data = data_write ? data_write_values : `TRACK_SENSOR_COUNT'bz;

task do_reset; begin
    envelope <= 0;
    data <= 0;
    ready <= 0;

    clock_write <= 1;
    clock_write_values <= ALL_HIGH;
    data_write <= 1;
    data_write_values <= ALL_LOW;

    stage <= START_CONFIG_STAGE;
    sub_stage <= 0;

    clock_counter <= 0;

    config_data <= 14'b00010010011001;
end endtask

task do_start_config; begin
    case (sub_stage)
    0: begin
        clock_write_values <= ALL_LOW;
        sub_stage <= 1;
    end
    1: begin
        data_write_values <= ALL_HIGH;
        sub_stage <= 2;
    end
    2: begin
        clock_write_values <= ALL_HIGH;
        sub_stage <= 3;
    end
    3: begin
        data_write_values <= ALL_LOW;
        stage <= START_WRITE_CONFIG_STAGE;
        sub_stage <= 0;
    end
    endcase
end endtask

task do_start_write_config; begin
    case (sub_stage)
    0: begin
        data_write_values <= ALL_HIGH;
        sub_stage <= 1;
    end
    1: begin
        data_write_values <= ALL_LOW;
        sub_stage <= 2;
    end
    2: begin
        clock_write_values <= ALL_LOW;
        sub_stage <= 3;
    end
    3: begin
        clock_write_values <= ALL_HIGH;
        stage <= WRITE_CONFIG_STAGE;
        sub_stage <= 0;
    end
    endcase
end endtask

task do_write_config; begin
    if (clock_write_values[0]) begin
        for (i = 0; i < `TRACK_SENSOR_COUNT; i = i + 1) begin
            data_write_values[i] <= config_data[sub_stage];
        end

        sub_stage <= sub_stage + 1;
    end else begin
        if (sub_stage == CONFIG_LENGTH) begin
            stage <= END_WRITE_CONFIG_STAGE;
            sub_stage <= 0;
        end
    end

    clock_write_values <= ~clock_write_values;
end endtask

task do_end_write_config; begin
    case (sub_stage)
    0: begin
        clock_write_values <= ALL_LOW;
        data_write_values <= ALL_LOW;
        sub_stage <= 1;
    end
    1: begin
        clock_write_values <= ALL_HIGH;
        stage <= END_CONFIG_STAGE;
        sub_stage <= 0;
    end
    endcase
end endtask

task do_end_config; begin
    case (sub_stage)
    0: begin
        data_write_values <= ALL_HIGH;
        sub_stage <= 1;
    end
    1: begin
        clock_write_values <= ALL_LOW;
        sub_stage <= 2;
    end
    2: begin
        data_write_values <= ALL_LOW;
        sub_stage <= 3;
    end
    3: begin
        ready <= 1;
        clock_write <= 0;
        data_write <= 0;
        stage <= LISTEN_STAGE;
        sub_stage <= 0;
    end
    endcase
end endtask

task do_config; begin
    case (stage)
    default: begin end
    START_CONFIG_STAGE: do_start_config();
    START_WRITE_CONFIG_STAGE: do_start_write_config();
    WRITE_CONFIG_STAGE: do_write_config();
    END_WRITE_CONFIG_STAGE: do_end_write_config();
    END_CONFIG_STAGE: do_end_config();
    endcase
end endtask

task do_listen; begin
    for (i = 0; i < `TRACK_SENSOR_COUNT; i = i + 1) begin
        envelope[i] <= !clock_write_values[i];
        data[i] <= data_write_values[i];
    end
end endtask

task do_main; begin
    if (clock_counter == (stage != LISTEN_STAGE ? BUS_CLOCK_DIVISION : SAMPLE_CLOCK_DIVISION)) begin
        if (stage != LISTEN_STAGE) begin
            do_config();
        end else begin
            do_listen();
        end

        clock_counter <= 0;
    end else begin
        clock_counter <= clock_counter + 1;
    end
end endtask

always @ (posedge clock) begin
    if (reset) begin
        do_reset();
    end else begin
        do_main();
    end
end

endmodule

`endif
