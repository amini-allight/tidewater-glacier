/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
`ifndef MIPI_HS_TRANSMITTER_V
`define MIPI_HS_TRANSMITTER_V

module mipi_hs_transmitter(
    clock,
    reset,

    enabled,
    seek_start,
    data,

    mipi_clock,
    mipi_lanes
);

input wire clock;
input wire reset;

input wire enabled;
input wire seek_start;
input wire [7:0] data[BYTE_WIDTH - 1:0];

output reg mipi_clock;
output reg [LANE_COUNT - 1:0] mipi_lanes;

parameter BYTE_WIDTH = 64;
parameter LANE_COUNT = 4;

localparam [2:0] FINAL_BIT_INDEX = 7;

integer i;

reg [2:0] bit_index;
reg [$clog2(BYTE_WIDTH) - 1:0] byte_index;

task do_reset; begin
    mipi_clock <= 0;
    mipi_lanes <= 0;

    bit_index <= 0;
    byte_index <= 0;
end endtask

task do_seek_start; begin
    bit_index <= 0;
    byte_index <= 0;
end endtask

task do_main; begin
    // TODO: MIPI: Clock oscillates at half the desired speed...
    mipi_clock <= ~mipi_clock;

    for (i = 0; i < LANE_COUNT; i++) begin
        mipi_lanes[i] <= data[byte_index + $clog2(BYTE_WIDTH)'(i)][bit_index];
    end

    bit_index <= bit_index + 1;

    if (bit_index == FINAL_BIT_INDEX) begin
        byte_index <= byte_index + LANE_COUNT;
    end
end endtask

always @ (posedge clock, negedge clock) begin
    if (reset) begin
        do_reset();
    end else if (enabled) begin
        if (seek_start) begin
            do_seek_start();
        end else begin
            do_main();
        end
    end
end

endmodule

`endif
