/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
`ifndef USB_ENCODER_128B132B_V
`define USB_ENCODER_128B132B_V

`include "constants.v"

// Encoded data is available on every 16th clock, new data to encode should be
// supplied on the same clock
module usb_128b132b_encoder(
    clock,
    reset,

    training,

    control,
    raw_symbols,

    encoded_data
);

input wire clock;
input wire reset;

input wire training;

input wire control;
input wire [7:0] raw_symbols[15:0];

output wire [131:0] encoded_data;

localparam BYTE_COUNT = 16;

integer i;

reg [3:0] preamble;
reg [7:0] encoded_symbols[15:0];

reg [15:0] lfsr;
reg [3:0] sub_stage;

assign encoded_data[3:0] = preamble;
generate for (genvar i = 0; i < BYTE_COUNT; i++) begin
    assign encoded_data[((i + 1) * 8 + 4) - 1:i * 8 + 4] = encoded_symbols[i];
end endgenerate

`include "common/usb_128b132b_encoding.v"

task do_reset; begin
    preamble <= `USB_128B132B_DATA_FLAG;
    for (i = 0; i < BYTE_COUNT; i++) begin
        encoded_symbols[i] <= 0;
    end

    lfsr <= 16'hffff;
    sub_stage <= 0;
end endtask

task do_main; begin
    if (sub_stage == 0) begin
        preamble <= control ? `USB_128B132B_CONTROL_FLAG : `USB_128B132B_DATA_FLAG;
    end

    encoded_symbols[sub_stage] <= scramble(raw_symbols[sub_stage]);
    sub_stage <= sub_stage + 1;

    do_advance();
end endtask

always @ (posedge clock) begin
    if (reset) begin
        do_reset();
    end else begin
        do_main();
    end
end

endmodule

`endif
