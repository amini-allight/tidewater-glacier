/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
/* Adapted from UG974 Vivado Ultrascale Libraries Guide */
`ifdef VERILATOR
`ifndef OBUFDS_DPHY_V
`define OBUFDS_DPHY_V

module OBUFDS_DPHY(
    HSTX_I,
    HSTX_T,
    LPTX_I_N,
    LPTX_I_P,
    LPTX_T,
    O,
    OB
);

input wire HSTX_I;
input wire HSTX_T;
input wire LPTX_I_N;
input wire LPTX_I_P;
input wire LPTX_T;
output reg O;
output reg OB;

parameter IOSTANDARD = "DEFAULT"; // Unimplemented

always @ (*) begin
    if (!HSTX_T) begin
        O = HSTX_I;
        OB = !HSTX_I;
    end else if (!LPTX_T) begin
        O = LPTX_I_P;
        OB = LPTX_I_N;
    end else begin
        O = 1'bz;
        OB = 1'bz;
    end
end

endmodule

`endif
`endif
