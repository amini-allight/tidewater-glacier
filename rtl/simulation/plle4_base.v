/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
/* Adapted from UG974 Vivado Ultrascale Libraries Guide */
`ifdef VERILATOR
`ifndef PLLE4_BASE_V
`define PLLE4_BASE_V

/* Simulation of parts of this for testing purposes */
module PLLE4_BASE(
   CLKFBOUT,
   CLKOUT0,
   CLKOUT0B,
   CLKOUT1,
   CLKOUT1B,
   CLKOUTPHY,
   LOCKED,
   CLKFBIN,
   CLKIN,
   CLKOUTPHYEN,
   PWRDWN,
   RST
);

// Phase and duty cycle are unimplemented and the clock pulses are not
// necessarily synced up with those of the inputs, couldn't figure out how to
// do these in verilog

output reg CLKFBOUT;
output reg CLKOUT0;
output reg CLKOUT0B;
output reg CLKOUT1;
output reg CLKOUT1B;
output reg CLKOUTPHY;
output reg LOCKED;
input wire CLKFBIN;
input wire CLKIN;
input wire CLKOUTPHYEN;
input wire PWRDWN;
input wire RST;

parameter CLKFBOUT_MULT = 5.0;
parameter CLKFBOUT_PHASE = 0.0;
parameter CLKIN_PERIOD = 0.0;
parameter CLKOUT0_DIVIDE = 1;
parameter CLKOUT0_DUTY_CYCLE = 0.5;
parameter CLKOUT0_PHASE = 0.0;
parameter CLKOUT1_DIVIDE = 1;
parameter CLKOUT1_DUTY_CYCLE = 0.5;
parameter CLKOUT1_PHASE = 0.0;
parameter CLKOUTPHY_MODE = "VCO_2X";
parameter DIVCLK_DIVIDE = 1;
parameter IS_CLKFBIN_INVERTED = 1'b0;
parameter IS_CLKIN_INVERTED = 1'b0;
parameter IS_PWRDWN_INVERTED = 1'b0;
parameter IS_RST_INVERTED = 1'b0;
parameter REF_JITTER = 0.0;
parameter STARTUP_WAIT = "FALSE";

// Divide by two because we need to flip the bit twice for every clock cycle
localparam VCO_PERIOD = ((CLKIN_PERIOD * CLKFBOUT_MULT) / DIVCLK_DIVIDE) / 2;
localparam CLOCK_0_PERIOD = VCO_PERIOD / CLKOUT0_DIVIDE;
localparam CLOCK_1_PERIOD = VCO_PERIOD / CLKOUT1_DIVIDE;

wire reset;
wire power_down;

assign reset = IS_RST_INVERTED ? !RST : RST;
assign power_down = IS_PWRDWN_INVERTED ? !PWRDWN : PWRDWN;

always begin
    if (!reset && !power_down) begin
        #(VCO_PERIOD) CLKFBOUT = ~CLKFBOUT;

        #(CLOCK_0_PERIOD) CLKOUT0 = ~CLKOUT0;
        #(CLOCK_0_PERIOD) CLKOUT0B = ~CLKOUT0B;

        #(CLOCK_1_PERIOD) CLKOUT1 = ~CLKOUT1;
        #(CLOCK_1_PERIOD) CLKOUT1B = ~CLKOUT1B;

        if (CLKOUTPHYEN) begin
            if (64'(CLKOUTPHY_MODE) == 64'("VCO")) begin
                #(VCO_PERIOD) CLKOUTPHY = ~CLKOUTPHY;
            end else if (64'(CLKOUTPHY_MODE) == 64'("VCO_2X")) begin
                #(VCO_PERIOD * 2) CLKOUTPHY = ~CLKOUTPHY;
            end else if (64'(CLKOUTPHY_MODE) == 64'("VCO_HALF")) begin
                #(VCO_PERIOD / 2) CLKOUTPHY = ~CLKOUTPHY;
            end
        end

        LOCKED = 1;
    end else begin
        CLKFBOUT = 0;
        CLKOUT0 = 0;
        CLKOUT0B = 1;
        CLKOUT1 = 0;
        CLKOUT1B = 1;
        CLKOUTPHY = 0;
        LOCKED = 0;
    end
end

endmodule

`endif
`endif
