/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
/* Adapted from UG974 Vivado Ultrascale Libraries Guide */
`ifdef VERILATOR
`ifndef IBUFDS_V
`define IBUFDS_V

module IBUFDS(
    I,
    IB,
    O
);

input wire I;
input wire IB; // Unimplemented

output wire O;

assign O = I;

endmodule

`endif
`endif
