/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
/* Adapted from UG974 Vivado Ultrascale Libraries Guide */
`ifdef VERILATOR
`ifndef STARTUPE3_V
`define STARTUPE3_V

/* Simulation of this as a clock source for testing purposes */
module STARTUPE3(
    CFGCLK,
    CFGMCLK,
    DI,
    DO,
    DTS,
    EOS,
    FCSBO,
    FCSBTS,
    GSR,
    GTS,
    KEYCLEARB,
    PACK,
    PREQ,
    USRCCLKO,
    USRCCLKTS,
    USRDONEO,
    USRDONETS
);

output wire CFGCLK;
output wire CFGMCLK; // Unimplemented

output wire [3:0] DI; // Unimplemented
input wire [3:0] DO; // Unimplemented
input wire [3:0] DTS; // Unimplemented

output wire EOS; // Unimplemented

input wire FCSBO; // Unimplemented
input wire FCSBTS; // Unimplemented

input wire GSR; // Unimplemented
input wire GTS; // Unimplemented

input wire KEYCLEARB; // Unimplemented

input wire PACK; // Unimplemented
output wire PREQ; // Unimplemented

input wire USRCCLKO; // Unimplemented
input wire USRCCLKTS; // Unimplemented
input wire USRDONEO; // Unimplemented
input wire USRDONETS; // Unimplemented

parameter PROG_USR = "FALSE"; // Unimplemented
parameter SIM_CCLK_FREQ = 0.0;

reg clock;

initial begin
    clock = 0;
end

always begin
    #(SIM_CCLK_FREQ / 2) clock = ~clock;
end

assign CFGCLK = clock;
assign CFGMCLK = 0;
assign DI = 0;
assign EOS = 1;
assign PREQ = 0;

endmodule

`endif
`endif
