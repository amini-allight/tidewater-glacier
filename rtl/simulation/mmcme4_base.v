/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
/* Adapted from UG974 Vivado Ultrascale Libraries Guide */
`ifdef VERILATOR
`ifndef MMCME4_BASE_V
`define MMCME4_BASE_V

module MMCME4_BASE(
   CLKFBOUT,
   CLKFBOUTB,
   CLKOUT0,
   CLKOUT0B,
   CLKOUT1,
   CLKOUT1B,
   CLKOUT2,
   CLKOUT2B,
   CLKOUT3,
   CLKOUT3B,
   CLKOUT4,
   CLKOUT5,
   CLKOUT6,
   LOCKED,
   CLKFBIN,
   CLKIN1,
   PWRDWN,
   RST
);

// Phase and duty cycle are unimplemented and the clock pulses are not
// necessarily synced up with those of the inputs, couldn't figure out how to
// do these in verilog

output reg CLKFBOUT;
output reg CLKFBOUTB;
output reg CLKOUT0;
output reg CLKOUT0B;
output reg CLKOUT1;
output reg CLKOUT1B;
output reg CLKOUT2;
output reg CLKOUT2B;
output reg CLKOUT3;
output reg CLKOUT3B;
output reg CLKOUT4;
output reg CLKOUT5;
output reg CLKOUT6;
output reg LOCKED;
input wire CLKFBIN;
input wire CLKIN1;
input wire PWRDWN;
input wire RST;

parameter BANDWIDTH = "OPTIMIZED";
parameter CLKFBOUT_MULT_F = 5.0;
parameter CLKFBOUT_PHASE = 0.0;
parameter CLKIN1_PERIOD = 0.0;
parameter CLKOUT0_DIVIDE_F = 1.0;
parameter CLKOUT0_DUTY_CYCLE = 0.5;
parameter CLKOUT0_PHASE = 0.0;
parameter CLKOUT1_DIVIDE = 1;
parameter CLKOUT1_DUTY_CYCLE = 0.5;
parameter CLKOUT1_PHASE = 0.0;
parameter CLKOUT2_DIVIDE = 1;
parameter CLKOUT2_DUTY_CYCLE = 0.5;
parameter CLKOUT2_PHASE = 0.0;
parameter CLKOUT3_DIVIDE = 1;
parameter CLKOUT3_DUTY_CYCLE = 0.5;
parameter CLKOUT3_PHASE = 0.0;
parameter CLKOUT4_CASCADE = "FALSE";
parameter CLKOUT4_DIVIDE = 1;
parameter CLKOUT4_DUTY_CYCLE = 0.5;
parameter CLKOUT4_PHASE = 0.0;
parameter CLKOUT5_DIVIDE = 1;
parameter CLKOUT5_DUTY_CYCLE = 0.5;
parameter CLKOUT5_PHASE = 0.0;
parameter CLKOUT6_DIVIDE = 1;
parameter CLKOUT6_DUTY_CYCLE = 0.5;
parameter CLKOUT6_PHASE = 0.0;
parameter DIVCLK_DIVIDE = 1;
parameter IS_CLKFBIN_INVERTED = 1'b0;
parameter IS_CLKIN1_INVERTED = 1'b0;
parameter IS_PWRDWN_INVERTED = 1'b0;
parameter IS_RST_INVERTED = 1'b0;
parameter REF_JITTER1 = 0.0;
parameter STARTUP_WAIT = "FALSE";

// Divide by two because we need to flip the bit twice for every clock cycle
localparam VCO_PERIOD = ((CLKIN1_PERIOD * CLKFBOUT_MULT_F) / DIVCLK_DIVIDE) / 2;
localparam CLOCK_0_PERIOD = VCO_PERIOD / CLKOUT0_DIVIDE_F;
localparam CLOCK_1_PERIOD = VCO_PERIOD / CLKOUT1_DIVIDE;
localparam CLOCK_2_PERIOD = VCO_PERIOD / CLKOUT2_DIVIDE;
localparam CLOCK_3_PERIOD = VCO_PERIOD / CLKOUT3_DIVIDE;
localparam CLOCK_4_PERIOD = VCO_PERIOD / (CLKOUT4_DIVIDE * (CLKOUT4_CASCADE == "TRUE" ? CLKOUT6_DIVIDE : 1));
localparam CLOCK_5_PERIOD = VCO_PERIOD / CLKOUT5_DIVIDE;
localparam CLOCK_6_PERIOD = VCO_PERIOD / CLKOUT6_DIVIDE;

wire reset;
wire power_down;

assign reset = IS_RST_INVERTED ? !RST : RST;
assign power_down = IS_PWRDWN_INVERTED ? !PWRDWN : PWRDWN;

always begin
    if (!reset && !power_down) begin
        LOCKED = 1;

        #(VCO_PERIOD) CLKFBOUT = ~CLKFBOUT;
        #(VCO_PERIOD) CLKFBOUTB = ~CLKFBOUTB;

        #(CLOCK_0_PERIOD) CLKOUT0 = ~CLKOUT0;
        #(CLOCK_0_PERIOD) CLKOUT0B = ~CLKOUT0B;

        #(CLOCK_1_PERIOD) CLKOUT1 = ~CLKOUT1;
        #(CLOCK_1_PERIOD) CLKOUT1B = ~CLKOUT1B;

        #(CLOCK_2_PERIOD) CLKOUT2 = ~CLKOUT2;
        #(CLOCK_2_PERIOD) CLKOUT2B = ~CLKOUT2B;

        #(CLOCK_3_PERIOD) CLKOUT3 = ~CLKOUT3;
        #(CLOCK_3_PERIOD) CLKOUT3B = ~CLKOUT3B;

        #(CLOCK_4_PERIOD) CLKOUT4 = ~CLKOUT4;
        #(CLOCK_5_PERIOD) CLKOUT5 = ~CLKOUT5;
        #(CLOCK_6_PERIOD) CLKOUT6 = ~CLKOUT6;
    end else begin
        CLKFBOUT = 0;
        CLKFBOUTB = 1;
        CLKOUT0 = 0;
        CLKOUT0B = 1;
        CLKOUT1 = 0;
        CLKOUT1B = 1;
        CLKOUT2 = 0;
        CLKOUT2B = 1;
        CLKOUT3 = 0;
        CLKOUT3B = 1;
        CLKOUT4 = 0;
        CLKOUT5 = 0;
        CLKOUT6 = 0;
        LOCKED = 0;
    end
end

endmodule

`endif
`endif
