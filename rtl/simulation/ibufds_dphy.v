/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
/* Adapted from UG974 Vivado Ultrascale Libraries Guide */
`ifdef VERILATOR
`ifndef IBUFDS_DPHY_V
`define IBUFDS_DPHY_V

module IBUFDS_DPHY(
    HSRX_DISABLE,
    HSRX_O,
    I,
    IB,
    LPRX_DISABLE,
    LPRX_O_N,
    LPRX_O_P
);

output reg HSRX_O;
input wire HSRX_DISABLE;
output reg LPRX_O_N;
output reg LPRX_O_P;
input wire LPRX_DISABLE;
input wire I;
input wire IB;

parameter DIFF_TERM = "TRUE"; // Unimplemented
parameter IOSTANDARD = "DEFAULT"; // Unimplemented
parameter SIM_DEVICE = "ULTRASCALE_PLUS"; // Unimplemented

always @ (*) begin
    if (!HSRX_DISABLE) begin
        HSRX_O = I;
        LPRX_O_N = 1'bz;
        LPRX_O_P = 1'bz;
    end else if (!LPRX_DISABLE) begin
        HSRX_O = 1'bz;
        LPRX_O_N = IB;
        LPRX_O_P = I;
    end else begin
        HSRX_O = 1'bz;
        LPRX_O_N = 1'bz;
        LPRX_O_P = 1'bz;
    end
end

endmodule

`endif
`endif
