/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
`ifndef CONTROLLER_V
`define CONTROLLER_V

`include "constants.v"

module controller(
    clock,
    reset,

    in_packet_valid,
    in_packet,
    in_packet_size,

    out_packet_valid,
    out_packet,
    out_packet_size,

    screen_l_scanline,
    screen_r_scanline,
    video_enabled,
    video_ready,
    video_error,

    eye_l_scanline,
    eye_r_scanline,
    mouth_scanline,
    pass_l_scanline,
    pass_r_scanline,

    audio_in,
    audio_out,
    audio_enabled,
    audio_ready,
    audio_error,

    track_envelope,
    track_data,
    track_ready,

    eye_relief,
    ipd,
    adjustment_error,

    imu_data,
    imu_error
);

input wire clock;
input wire reset;

input wire in_packet_valid;
input wire [`USB_MAX_PACKET_SIZE - 1:0] in_packet;
input wire [`USB_PACKET_SIZE_SIZE - 1:0] in_packet_size;

output reg out_packet_valid;
output reg [`USB_MAX_PACKET_SIZE - 1:0] out_packet;
output reg [`USB_PACKET_SIZE_SIZE - 1:0] out_packet_size;

output reg [`SCREEN_SCANLINE_SIZE - 1:0] screen_l_scanline;
output reg [`SCREEN_SCANLINE_SIZE - 1:0] screen_r_scanline;
output reg video_enabled;
input wire video_ready;
input wire video_error;

input wire [`FACE_TRACK_SCANLINE_SIZE - 1:0] eye_l_scanline;
input wire [`FACE_TRACK_SCANLINE_SIZE - 1:0] eye_r_scanline;
input wire [`FACE_TRACK_SCANLINE_SIZE - 1:0] mouth_scanline;
input wire [`PASSTHROUGH_SCANLINE_SIZE - 1:0] pass_l_scanline;
input wire [`PASSTHROUGH_SCANLINE_SIZE - 1:0] pass_r_scanline;

input wire [`AUDIO_BIT_DEPTH - 1:0] audio_in[`AUDIO_FRAME_SAMPLES * `AUDIO_CHANNELS - 1:0];
output reg [`AUDIO_BIT_DEPTH - 1:0] audio_out[`AUDIO_FRAME_SAMPLES * `AUDIO_CHANNELS - 1:0];
output reg audio_enabled;
input wire audio_ready;
input wire audio_error;

input wire [`TRACK_SENSOR_COUNT - 1:0] track_envelope;
input wire [`TRACK_SENSOR_COUNT - 1:0] track_data;
input wire track_ready;

input wire [7:0] eye_relief;
input wire [7:0] ipd;
input wire adjustment_error;

input wire [`IMU_WORD_SIZE - 1:0] imu_data[5:0];
input wire imu_error;

parameter CLOCK_FREQ = 122400000;

localparam [7:0] AUDIO_PACKET = 0;
localparam [7:0] TRACK_PACKET = 1;
localparam [7:0] IMU_PACKET = 2;
localparam [7:0] ADJUST_PACKET = 3;
localparam [7:0] VIDEO_SCREEN_L_PACKET = 4;
localparam [7:0] VIDEO_SCREEN_R_PACKET = 5;
localparam [7:0] VIDEO_EYE_L_PACKET = 6;
localparam [7:0] VIDEO_EYE_R_PACKET = 7;
localparam [7:0] VIDEO_MOUTH_PACKET = 8;
localparam [7:0] VIDEO_PASS_L_PACKET = 9;
localparam [7:0] VIDEO_PASS_R_PACKET = 10;

localparam [11:0] CLOCK_DIVISION_48KHZ = 12'(CLOCK_FREQ / `AUDIO_FREQ);
localparam [16:0] CLOCK_DIVISION_1KHZ = 17'(CLOCK_FREQ / 1000);
localparam [20:0] CLOCK_DIVISION_100HZ = 21'(CLOCK_FREQ / 100);
localparam [14:0] CLOCK_DIVISION_90HZ = 15'(CLOCK_FREQ / `SCREEN_FRAME_RATE);
localparam [14:0] CLOCK_DIVISION_60HZ = 15'(CLOCK_FREQ / `CAMERA_FRAME_RATE);

integer i;

wire [7:0] packet_type;

reg [11:0] clock_counter_48khz;
reg [16:0] clock_counter_1khz;
reg [20:0] clock_counter_100hz;
reg [14:0] clock_counter_90hz;
reg [14:0] clock_counter_60hz;

reg track_needed;
reg adjust_needed;
reg imu_needed;
reg eye_l_needed;
reg eye_r_needed;
reg mouth_needed;
reg pass_l_needed;
reg pass_r_needed;

reg [`SCREEN_SCANLINE_INDEX_SIZE - 1:0] screen_l_write_head;
reg [`SCREEN_SCANLINE_INDEX_SIZE - 1:0] screen_r_write_head;
reg [`FACE_TRACK_SCANLINE_INDEX_SIZE - 1:0] eye_l_read_head;
reg [`FACE_TRACK_SCANLINE_INDEX_SIZE - 1:0] eye_r_read_head;
reg [`FACE_TRACK_SCANLINE_INDEX_SIZE - 1:0] mouth_read_head;
reg [`PASSTHROUGH_SCANLINE_INDEX_SIZE - 1:0] pass_l_read_head;
reg [`PASSTHROUGH_SCANLINE_INDEX_SIZE - 1:0] pass_r_read_head;

reg [`SCREEN_SCANLINE_SIZE - 1:0] screen_l_scanline_staging;
reg [`SCREEN_SCANLINE_SIZE - 1:0] screen_r_scanline_staging;
reg [`AUDIO_BIT_DEPTH - 1:0] audio_out_staging[`AUDIO_FRAME_SAMPLES * `AUDIO_CHANNELS - 1:0];

assign packet_type = in_packet[7:0];

task do_reset; begin
    out_packet_valid <= 0;
    out_packet <= 0;
    out_packet_size <= 0;

    screen_l_scanline <= 0;
    screen_r_scanline <= 0;
    video_enabled <= 0;

    for (i = 0; i < `AUDIO_FRAME_SAMPLES * `AUDIO_CHANNELS; i = i + 1) begin
        audio_out[i] <= 0;
    end
    audio_enabled <= 0;

    // This is deliberate to cause this module to hit its audio step one clock
    // later than the audio_controller, so the data is ready to read for
    // output
    clock_counter_48khz <= CLOCK_DIVISION_48KHZ;
    clock_counter_1khz <= 0;
    clock_counter_100hz <= 0;
    clock_counter_90hz <= 0;
    clock_counter_60hz <= 0;

    track_needed <= 0;
    adjust_needed <= 0;
    imu_needed <= 0;
    eye_l_needed <= 0;
    eye_r_needed <= 0;
    mouth_needed <= 0;
    pass_l_needed <= 0;
    pass_r_needed <= 0;

    screen_l_write_head <= 0;
    screen_r_write_head <= 0;
    eye_l_read_head <= 0;
    eye_r_read_head <= 0;
    mouth_read_head <= 0;
    pass_l_read_head <= 0;
    pass_l_read_head <= 0;

    screen_l_scanline_staging <= 0;
    screen_r_scanline_staging <= 0;
    for (i = 0; i < `AUDIO_FRAME_SAMPLES * `AUDIO_CHANNELS; i = i + 1) begin
        audio_out_staging[i] <= 0;
    end
end endtask

task do_in_video_screen_l; begin
    for (i = 0; i < `USB_MAX_PACKET_BYTES; i = i + 1) begin
        if (i < in_packet_size) begin
            screen_l_scanline_staging[32'(screen_l_write_head) + i+:8] <= in_packet[i+:8];
        end
    end

    if (32'(screen_l_write_head) + 32'(in_packet_size) == `SCREEN_SCANLINE_BYTES) begin
        screen_l_write_head <= 0;
    end else begin
        screen_l_write_head <= screen_l_write_head + `SCREEN_SCANLINE_INDEX_SIZE'(in_packet_size);
    end

    if (video_ready && screen_l_write_head == 0) begin
        video_enabled <= 1;
    end
end endtask

task do_in_video_screen_r; begin
    for (i = 0; i < `USB_MAX_PACKET_BYTES; i = i + 1) begin
        if (i < in_packet_size) begin
            screen_r_scanline_staging[32'(screen_r_write_head) + i+:8] <= in_packet[i+:8];
        end
    end

    if (32'(screen_r_write_head) + 32'(in_packet_size) == `SCREEN_SCANLINE_BYTES) begin
        screen_r_write_head <= 0;
    end else begin
        screen_r_write_head <= screen_r_write_head + `SCREEN_SCANLINE_INDEX_SIZE'(in_packet_size);
    end

    if (video_ready && screen_r_write_head == 0) begin
        video_enabled <= 1;
    end
end endtask

task do_in_audio; begin
    for (i = 0; i < `AUDIO_FRAME_SAMPLES * `AUDIO_CHANNELS; i = i + 1) begin
        audio_out_staging[i] <= in_packet[8 + i * `AUDIO_BIT_DEPTH+:`AUDIO_BIT_DEPTH];
    end

    if (audio_ready) begin
        audio_enabled <= 1;
    end
end endtask

task do_in_packet; begin
    if (in_packet_valid) begin
        case (packet_type)
        default: begin end
        VIDEO_SCREEN_L_PACKET: do_in_video_screen_l();
        VIDEO_SCREEN_R_PACKET: do_in_video_screen_r();
        AUDIO_PACKET: do_in_audio();
        endcase
    end
end endtask

task do_out_video_eye_l; begin
    out_packet_valid <= 1;
    out_packet[7:0] <= VIDEO_EYE_L_PACKET;
    for (i = 1; i < `USB_MAX_PACKET_BYTES; i = i + 1) begin
        if (32'(eye_l_read_head) + (i - 1) < `FACE_TRACK_SCANLINE_BYTES) begin
            out_packet[i] <= eye_l_scanline[32'(eye_l_read_head) + (i - 1)];
        end
    end

    if (eye_l_read_head + (`USB_MAX_PACKET_BYTES - 1) >= `FACE_TRACK_SCANLINE_BYTES) begin
        out_packet_size <= `USB_PACKET_SIZE_SIZE'(1 + (`FACE_TRACK_SCANLINE_BYTES - 32'(eye_l_read_head)));
        eye_l_read_head <= 0;
        eye_l_needed <= 0;
    end else begin
        out_packet_size <= `USB_PACKET_SIZE_SIZE'(`USB_MAX_PACKET_BYTES);
        eye_l_read_head <= eye_l_read_head + (`USB_MAX_PACKET_BYTES - 1);
    end
end endtask

task do_out_video_eye_r; begin
    out_packet_valid <= 1;
    out_packet[7:0] <= VIDEO_EYE_R_PACKET;
    for (i = 1; i < `USB_MAX_PACKET_BYTES; i = i + 1) begin
        if (32'(eye_r_read_head) + (i - 1) < `FACE_TRACK_SCANLINE_BYTES) begin
            out_packet[i] <= eye_r_scanline[32'(eye_r_read_head) + (i - 1)];
        end
    end

    if (eye_r_read_head + (`USB_MAX_PACKET_BYTES - 1) >= `FACE_TRACK_SCANLINE_BYTES) begin
        out_packet_size <= `USB_PACKET_SIZE_SIZE'(1 + (`FACE_TRACK_SCANLINE_BYTES - 32'(eye_r_read_head)));
        eye_r_read_head <= 0;
        eye_r_needed <= 0;
    end else begin
        out_packet_size <= `USB_PACKET_SIZE_SIZE'(`USB_MAX_PACKET_BYTES);
        eye_r_read_head <= eye_r_read_head + (`USB_MAX_PACKET_BYTES - 1);
    end
end endtask

task do_out_video_mouth; begin
    out_packet_valid <= 1;
    out_packet[7:0] <= VIDEO_MOUTH_PACKET;
    for (i = 1; i < `USB_MAX_PACKET_BYTES; i = i + 1) begin
        if (32'(mouth_read_head) + (i - 1) < `FACE_TRACK_SCANLINE_BYTES) begin
            out_packet[i] <= mouth_scanline[32'(mouth_read_head) + (i - 1)];
        end
    end

    if (mouth_read_head + (`USB_MAX_PACKET_BYTES - 1) >= `FACE_TRACK_SCANLINE_BYTES) begin
        out_packet_size <= `USB_PACKET_SIZE_SIZE'(1 + (`FACE_TRACK_SCANLINE_BYTES - 32'(mouth_read_head)));
        mouth_read_head <= 0;
        mouth_needed <= 0;
    end else begin
        out_packet_size <= `USB_PACKET_SIZE_SIZE'(`USB_MAX_PACKET_BYTES);
        mouth_read_head <= mouth_read_head + (`USB_MAX_PACKET_BYTES - 1);
    end
end endtask

task do_out_video_pass_l; begin
    out_packet_valid <= 1;
    out_packet[7:0] <= VIDEO_PASS_L_PACKET;
    for (i = 1; i < `USB_MAX_PACKET_BYTES; i = i + 1) begin
        if (32'(pass_l_read_head) + (i - 1) < `PASSTHROUGH_SCANLINE_BYTES) begin
            out_packet[i] <= pass_l_scanline[32'(pass_l_read_head) + (i - 1)];
        end
    end

    if (pass_l_read_head + (`USB_MAX_PACKET_BYTES - 1) >= `PASSTHROUGH_SCANLINE_BYTES) begin
        out_packet_size <= `USB_PACKET_SIZE_SIZE'(1 + (`PASSTHROUGH_SCANLINE_BYTES - 32'(pass_l_read_head)));
        pass_l_read_head <= 0;
        pass_l_needed <= 0;
    end else begin
        out_packet_size <= `USB_PACKET_SIZE_SIZE'(`USB_MAX_PACKET_BYTES);
        pass_l_read_head <= pass_l_read_head + (`USB_MAX_PACKET_BYTES - 1);
    end
end endtask

task do_out_video_pass_r; begin
    out_packet_valid <= 1;
    out_packet[7:0] <= VIDEO_PASS_R_PACKET;
    for (i = 1; i < `USB_MAX_PACKET_BYTES; i = i + 1) begin
        if (32'(pass_r_read_head) + (i - 1) < `PASSTHROUGH_SCANLINE_BYTES) begin
            out_packet[i] <= pass_r_scanline[32'(pass_r_read_head) + (i - 1)];
        end
    end

    if (pass_r_read_head + (`USB_MAX_PACKET_BYTES - 1) >= `PASSTHROUGH_SCANLINE_BYTES) begin
        out_packet_size <= `USB_PACKET_SIZE_SIZE'(1 + (`PASSTHROUGH_SCANLINE_BYTES - 32'(pass_r_read_head)));
        pass_r_read_head <= 0;
        pass_r_needed <= 0;
    end else begin
        out_packet_size <= `USB_PACKET_SIZE_SIZE'(`USB_MAX_PACKET_BYTES);
        pass_r_read_head <= pass_r_read_head + (`USB_MAX_PACKET_BYTES - 1);
    end
end endtask

task do_out_audio; begin
    out_packet_valid <= 1;
    out_packet[7:0] <= AUDIO_PACKET;
    for (i = 0; i < `AUDIO_FRAME_SAMPLES * `AUDIO_CHANNELS; i = i + 1) begin
        out_packet[8 + i * `AUDIO_BIT_DEPTH+:`AUDIO_BIT_DEPTH] <= audio_in[i];
    end
    out_packet_size <= `USB_PACKET_SIZE_SIZE'(`AUDIO_PACKET_SIZE);
end endtask

task do_out_track; begin
    out_packet_valid <= 1;
    out_packet[7:0] <= TRACK_PACKET;
    out_packet[23:8] <= track_envelope;
    out_packet[39:24] <= track_data;
    out_packet_size <= `USB_PACKET_SIZE_SIZE'(`TRACK_PACKET_SIZE);
    track_needed <= 0;
end endtask

task do_out_imu; begin
    out_packet_valid <= 1;
    out_packet[7:0] <= IMU_PACKET;
    for (i = 0; i < 6; i = i + 1) begin
        out_packet[8 + i * `IMU_WORD_SIZE+:`IMU_WORD_SIZE] <= imu_data[i];
    end
    out_packet_size <= `USB_PACKET_SIZE_SIZE'(`IMU_PACKET_SIZE);
    imu_needed <= 0;
end endtask

task do_out_adjust; begin
    out_packet_valid <= 1;
    out_packet[7:0] <= ADJUST_PACKET;
    out_packet[15:8] <= eye_relief;
    out_packet[23:16] <= ipd;
    out_packet_size <= `USB_PACKET_SIZE_SIZE'(`ADJUST_PACKET_SIZE);
    adjust_needed <= 0;
end endtask

task do_video_staging; begin
    screen_l_scanline <= screen_l_scanline_staging;
    screen_r_scanline <= screen_r_scanline_staging;
end endtask

task do_audio_staging; begin
    for (i = 0; i < `AUDIO_FRAME_SAMPLES * `AUDIO_CHANNELS; i = i + 1) begin
        audio_out[i] <= audio_out_staging[i];
    end
end endtask

task do_out_packet; begin
    if (audio_enabled) begin
        if (clock_counter_48khz == CLOCK_DIVISION_48KHZ) begin
            do_out_audio();
            clock_counter_48khz <= 0;
        end else begin
            if (clock_counter_48khz + 1 == CLOCK_DIVISION_48KHZ) begin
                do_audio_staging();
            end
            out_packet_valid <= 0;
            clock_counter_48khz <= clock_counter_48khz + 1;
        end
    end

    if (clock_counter_1khz == CLOCK_DIVISION_1KHZ) begin
        if (track_ready) begin
            track_needed <= 1;
        end
        imu_needed <= 1;
        clock_counter_1khz <= 0;
    end else begin
        clock_counter_1khz <= clock_counter_1khz + 1;
    end

    if (clock_counter_100hz == CLOCK_DIVISION_100HZ) begin
        adjust_needed <= 1;
        clock_counter_100hz <= 0;
    end else begin
        clock_counter_100hz <= clock_counter_100hz + 1;
    end

    if (video_enabled) begin
        if (clock_counter_90hz == CLOCK_DIVISION_90HZ) begin
            clock_counter_90hz <= 0;
        end else begin
            if (clock_counter_90hz + 1 == CLOCK_DIVISION_90HZ) begin
                do_video_staging();
            end
            clock_counter_90hz <= clock_counter_90hz + 1;
        end
    end

    if (clock_counter_60hz == CLOCK_DIVISION_60HZ) begin
        eye_l_needed <= 1;
        eye_r_needed <= 1;
        mouth_needed <= 1;
        pass_l_needed <= 1;
        pass_r_needed <= 1;
        clock_counter_60hz <= 0;
    end else begin
        clock_counter_60hz <= clock_counter_60hz + 1;
    end

    if (!audio_enabled || clock_counter_48khz != CLOCK_DIVISION_48KHZ) begin
        if (eye_l_needed) begin
            do_out_video_eye_l();
        end else if (eye_r_needed) begin
            do_out_video_eye_r();
        end else if (mouth_needed) begin
            do_out_video_mouth();
        end else if (pass_l_needed) begin
            do_out_video_pass_l();
        end else if (pass_r_needed) begin
            do_out_video_pass_r();
        end else if (track_needed) begin
            do_out_track();
        end else if (imu_needed) begin
            do_out_imu();
        end else if (adjust_needed) begin
            do_out_adjust();
        end else begin
            out_packet_valid <= 0;
        end
    end
end endtask

task do_main; begin
    do_in_packet();

    do_out_packet();
end endtask

always @ (posedge clock) begin
    if (reset) begin
        do_reset();
    end else begin
        do_main();
    end
end

endmodule

`endif
