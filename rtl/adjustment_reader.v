/* Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0 */
`ifndef ADJUSTMENT_READER_V
`define ADJUSTMENT_READER_V

`include "constants.v"

// Conversion begins when data_requested goes high, result will be available
// ~60 clocks (@ 400 kHz) and remain available until data_requested is next
// signalled
module adjustment_reader(
    clock,
    reset,

    data_requested,
    eye_relief,
    ipd,
    error,

    scl,
    sda
);

input wire clock;
input wire reset;

input wire data_requested;
output wire [7:0] eye_relief;
output wire [7:0] ipd;
output reg error;

output reg scl;
inout wire sda;

parameter CLOCK_FREQ = 122400000;

localparam [3:0] IDLE_STAGE = 0;
localparam [3:0] START_STAGE = 1;
localparam [3:0] ADDRESS_STAGE = 2;
localparam [3:0] ADDRESS_ACK_STAGE = 3;
localparam [3:0] READ_MSB_STAGE = 4;
localparam [3:0] READ_MSB_ACK_STAGE = 5;
localparam [3:0] READ_LSB_STAGE = 6;
localparam [3:0] READ_LSB_ACK_STAGE = 7;
localparam [3:0] STOP_STAGE = 8;

localparam [2:0] FINAL_BIT_INDEX = 7;

localparam [8:0] CLOCK_DIVISION = 9'(CLOCK_FREQ
    / `I2C_FAST_CLOCK_FREQ / 2);

// Has the read flag appended to the end
localparam [0:7] EYE_RELIEF_ADC_ADDRESS = 8'b10101101;
localparam [0:7] IPD_ADC_ADDRESS = 8'b10101011;

reg [0:7] eye_relief_big_endian;
reg [0:7] ipd_big_endian;

reg [3:0] stage;
reg [2:0] sub_stage;
reg go_next;
reg active_adc;

reg write;
reg write_value;

reg [8:0] clock_counter;

generate for (genvar i = 0; i < 8; i = i + 1) begin
    assign eye_relief[i] = eye_relief_big_endian[i];
    assign ipd[i] = ipd_big_endian[i];
end endgenerate

assign sda = write ? write_value : 1'bz;

task do_reset; begin
    error <= 0;

    scl <= 1;

    eye_relief_big_endian <= 0;
    ipd_big_endian <= 0;

    stage <= IDLE_STAGE;
    sub_stage <= 0;
    go_next <= 0;
    active_adc <= 0;

    write <= 1;
    write_value <= 1;

    clock_counter <= 0;
end endtask

task do_start; begin
    write_value <= 0;
    stage <= ADDRESS_STAGE;
end endtask

task do_address; begin
    if (!scl) begin
        if (!active_adc) begin
            write_value <= EYE_RELIEF_ADC_ADDRESS[sub_stage];
        end else begin
            write_value <= IPD_ADC_ADDRESS[sub_stage];
        end
        sub_stage <= sub_stage + 1;
        go_next <= (sub_stage == FINAL_BIT_INDEX);
    end else begin
        if (go_next) begin
            go_next <= 0;
            stage <= ADDRESS_ACK_STAGE;
        end
    end

    scl <= !scl;
end endtask

task do_address_ack; begin
    if (!scl) begin
        write <= 0;
        write_value <= 0;
    end else begin
        write <= 1;

        if (sda) begin
            error <= 1;
            stage <= STOP_STAGE;
        end else begin
            stage <= READ_MSB_STAGE;
        end
    end

    scl <= !scl;
end endtask

task do_read_msb; begin
    if (!scl) begin
        write <= 0;
    end else begin
        if (sub_stage >= 4) begin
            if (!active_adc) begin
                eye_relief_big_endian[sub_stage - 4] <= sda;
            end else begin
                ipd_big_endian[sub_stage - 4] <= sda;
            end
        end
        sub_stage <= sub_stage + 1;
        if (sub_stage == FINAL_BIT_INDEX) begin
            stage <= READ_MSB_ACK_STAGE;
        end
    end

    scl <= !scl;
end endtask

task do_read_msb_ack; begin
    if (!scl) begin
        write <= 1;
        write_value <= 0;
    end else begin
        stage <= READ_LSB_STAGE;
    end

    scl <= !scl;
end endtask

task do_read_lsb; begin
    if (!scl) begin
        write <= 0;
    end else begin
        if (sub_stage < 4) begin
            if (!active_adc) begin
                eye_relief_big_endian[sub_stage + 4] <= sda;
            end else begin
                ipd_big_endian[sub_stage + 4] <= sda;
            end
        end
        sub_stage <= sub_stage + 1;
        if (sub_stage == FINAL_BIT_INDEX) begin
            stage <= READ_LSB_ACK_STAGE;
        end
    end

    scl <= !scl;
end endtask

task do_read_lsb_ack; begin
    if (!scl) begin
        write <= 1;
        write_value <= 0;
    end else begin
        stage <= STOP_STAGE;
    end

    scl <= !scl;
end endtask

task do_stop; begin
    if (scl) begin
        write_value <= 1;
        if (!active_adc) begin
            stage <= START_STAGE;
        end else begin
            stage <= IDLE_STAGE;
        end
        active_adc <= !active_adc;
    end else begin
        scl <= 1;
        write_value <= 0;
    end
end endtask

task do_transmit; begin
    if (clock_counter == CLOCK_DIVISION) begin
        case (stage)
        default: begin end
        START_STAGE: do_start();
        ADDRESS_STAGE: do_address();
        ADDRESS_ACK_STAGE: do_address_ack();
        READ_MSB_STAGE: do_read_msb();
        READ_MSB_ACK_STAGE: do_read_msb_ack();
        READ_LSB_STAGE: do_read_lsb();
        READ_LSB_ACK_STAGE: do_read_lsb_ack();
        STOP_STAGE: do_stop();
        endcase

        clock_counter <= 0;
    end else begin
        clock_counter <= clock_counter + 1;
    end
end endtask

task do_main; begin
    if (data_requested) begin
        if (stage == IDLE_STAGE) begin
            stage <= START_STAGE;
        end
    end else begin
        do_transmit();
    end
end endtask

always @ (posedge clock) begin
    if (reset) begin
        do_reset();
    end else begin
        do_main();
    end
end

endmodule

`endif
