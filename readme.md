# Tidewater Glacier

The soft core for the [Ice Sea](https://amini-allight.org/ice-sea) VR headset. Designed to be run on the FPGA of the headset mainboard (not yet available).

## Testing

Testing is performed using [Verilator](https://www.veripool.org/verilator/) and [GTKWave](https://gtkwave.sourceforge.net/) and you will need both of them installed to do it successfully. To run a specific testbench use:

```sh
./test.sh audio_input
```

Replacing `audio_input` with the name of the testbench you want to run (minus the `_tb.v` suffix). A list of testbenches can be found in the `test` directory in the project root.

## Using

The design in this repository needs to be synthesized, implemented, converted to a bitstream and then written to an XCAU7P-1SBVC484E FPGA. This is done using [AMD/Xilinx Vivado](https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/vivado-design-tools.html) on the software side and an [AMD/Xilinx USB Platform Cable](https://www.xilinx.com/products/boards-and-kits/hw-usb-ii-g.html) on the hardware side. You will need these installed on/plugged into your computer to program your device successfully. To program your device use:

```sh
export VIVADO_BIN_PATH="/home/developer/Xilinx/Vivado/2024.1/bin"
./synth.sh
```

Replacing the path with the actual path to your Vivado install's `bin` directory.

## License

Developed by Amini Allight. Licensed under the [CERN Open Hardware Licence Version 2 - Weakly Reciprocal (CERN-OHL-W-2.0)](https://ohwr.org/project/cernohl/-/wikis/uploads/82b567f43ce515395f7ddbfbad7a8806/cern_ohl_w_v2.txt).

Contains designs based on various specifications including but not limited to USB. Such specifications are property of their respective owners who are not affiliated with this project and do not endorse it.
