# Copyright 2024 Amini Allight, licensed as CERN-OHL-W-2.0
set_property CFGBVS GND [current_design]
set_property CONFIG_VOLTAGE 1.8 [current_design]

set_property -dict { LOC E3 IOSTANDARD LVCMOS33 } [get_ports { audio_in_cs }];
set_property -dict { LOC A2 IOSTANDARD LVCMOS33 } [get_ports { audio_in_clock }];
set_property -dict { LOC B2 IOSTANDARD LVCMOS33 } [get_ports { audio_in_data_l }];
set_property -dict { LOC F5 IOSTANDARD LVCMOS33 } [get_ports { audio_in_data_r }];

set_property -dict { LOC C1 IOSTANDARD LVCMOS33 } [get_ports { audio_out_scl }];
set_property -dict { LOC C2 IOSTANDARD LVCMOS33 } [get_ports { audio_out_sda_l }];
set_property -dict { LOC F3 IOSTANDARD LVCMOS33 } [get_ports { audio_out_sda_r }];

set_property -dict { LOC AB4 IOSTANDARD LVCMOS33 } [get_ports { clock_resetn }];
set_property -dict { LOC AB2 IOSTANDARD LVCMOS33 } [get_ports { clock_scl }];
set_property -dict { LOC AB3 IOSTANDARD LVCMOS33 } [get_ports { clock_sda }];

set_property -dict { LOC H1 IOSTANDARD LVCMOS33 } [get_ports { usb_pd_scl }];
set_property -dict { LOC H2 IOSTANDARD LVCMOS33 } [get_ports { usb_pd_sda }];

set_property -dict { LOC AA8 IOSTANDARD LVCMOS33 } [get_ports { usb_data_p }];
set_property -dict { LOC AB8 IOSTANDARD LVCMOS33 } [get_ports { usb_data_n }];

set_property -dict { LOC AA7 IOSTANDARD LVCMOS33 } [get_ports { usb_pullup }];
set_property -dict { LOC AB7 IOSTANDARD LVCMOS33 } [get_ports { usb_orientation }];
set_property -dict { LOC AB5 IOSTANDARD LVCMOS33 } [get_ports { usb_open_transmit }];

set_property -dict { LOC J3 IOSTANDARD LVCMOS18 } [get_ports { screen_l_resetn }];
set_property -dict { LOC J2 IOSTANDARD LVCMOS18 } [get_ports { screen_l_aux[0] }];
set_property -dict { LOC G3 IOSTANDARD LVCMOS18 } [get_ports { screen_l_aux[1] }];
set_property -dict { LOC J1 IOSTANDARD LVCMOS18 } [get_ports { screen_l_aux[2] }];

set_property -dict { LOC H19 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { screen_l_clock_a_p }];
set_property -dict { LOC P16 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { screen_l_data_a_p[0] }];
set_property -dict { LOC E19 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { screen_l_data_a_p[1] }];
set_property -dict { LOC L20 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { screen_l_data_a_p[2] }];
set_property -dict { LOC K18 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { screen_l_data_a_p[3] }];

set_property -dict { LOC G20 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { screen_l_clock_a_n }];
set_property -dict { LOC P17 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { screen_l_data_a_n[0] }];
set_property -dict { LOC E20 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { screen_l_data_a_n[1] }];
set_property -dict { LOC K20 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { screen_l_data_a_n[2] }];
set_property -dict { LOC J18 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { screen_l_data_a_n[3] }];

set_property -dict { LOC R17 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { screen_l_clock_b_p }];
set_property -dict { LOC R15 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { screen_l_data_b_p[0] }];
set_property -dict { LOC N15 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { screen_l_data_b_p[1] }];
set_property -dict { LOC L17 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { screen_l_data_b_p[2] }];
set_property -dict { LOC K16 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { screen_l_data_b_p[3] }];

set_property -dict { LOC P18 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { screen_l_clock_b_n }];
set_property -dict { LOC R16 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { screen_l_data_b_n[0] }];
set_property -dict { LOC N16 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { screen_l_data_b_n[1] }];
set_property -dict { LOC L18 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { screen_l_data_b_n[2] }];
set_property -dict { LOC J17 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { screen_l_data_b_n[3] }];

set_property -dict { LOC U13 IOSTANDARD LVCMOS18 } [get_ports { screen_r_resetn }];
set_property -dict { LOC U10 IOSTANDARD LVCMOS18 } [get_ports { screen_r_aux[0] }];
set_property -dict { LOC T15 IOSTANDARD LVCMOS18 } [get_ports { screen_r_aux[1] }];
set_property -dict { LOC U12 IOSTANDARD LVCMOS18 } [get_ports { screen_r_aux[2] }];

set_property -dict { LOC A17 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { screen_r_clock_a_p }];
set_property -dict { LOC B18 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { screen_r_data_a_p[0] }];
set_property -dict { LOC E18 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { screen_r_data_a_p[1] }];
set_property -dict { LOC C21 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { screen_r_data_a_p[2] }];
set_property -dict { LOC E21 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { screen_r_data_a_p[3] }];

set_property -dict { LOC A18 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { screen_r_clock_a_n }];
set_property -dict { LOC B19 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { screen_r_data_a_n[0] }];
set_property -dict { LOC D19 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { screen_r_data_a_n[1] }];
set_property -dict { LOC B22 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { screen_r_data_a_n[2] }];
set_property -dict { LOC D22 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { screen_r_data_a_n[3] }];

set_property -dict { LOC M15 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { screen_r_clock_b_p }];
set_property -dict { LOC K15 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { screen_r_data_b_p[0] }];
set_property -dict { LOC H16 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { screen_r_data_b_p[1] }];
set_property -dict { LOC G15 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { screen_r_data_b_p[2] }];
set_property -dict { LOC G18 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { screen_r_data_b_p[3] }];

set_property -dict { LOC L16 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { screen_r_clock_b_n }];
set_property -dict { LOC J16 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { screen_r_data_b_n[0] }];
set_property -dict { LOC G17 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { screen_r_data_b_n[1] }];
set_property -dict { LOC F16 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { screen_r_data_b_n[2] }];
set_property -dict { LOC F18 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { screen_r_data_b_n[3] }];

set_property -dict { LOC E1 IOSTANDARD LVCMOS33 } [get_ports { camera_eye_l_power_en }];
set_property -dict { LOC F2 IOSTANDARD LVCMOS33 } [get_ports { camera_eye_l_xclk }];
set_property -dict { LOC F1 IOSTANDARD LVCMOS33 } [get_ports { camera_eye_l_scl }];
set_property -dict { LOC G2 IOSTANDARD LVCMOS33 } [get_ports { camera_eye_l_sda }];

set_property -dict { LOC D11 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_eye_l_clock_p }];
set_property -dict { LOC F13 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_eye_l_data_p[0] }];
set_property -dict { LOC F11 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_eye_l_data_p[1] }];
set_property -dict { LOC C10 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_eye_l_data_p[2] }];
set_property -dict { LOC A10 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_eye_l_data_p[3] }];

set_property -dict { LOC C11 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_eye_l_clock_n }];
set_property -dict { LOC E14 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_eye_l_data_n[0] }];
set_property -dict { LOC F12 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_eye_l_data_n[1] }];
set_property -dict { LOC B10 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_eye_l_data_n[2] }];
set_property -dict { LOC A11 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_eye_l_data_n[3] }];

set_property -dict { LOC AA15 IOSTANDARD LVCMOS33 } [get_ports { camera_eye_r_power_en }];
set_property -dict { LOC AB15 IOSTANDARD LVCMOS33 } [get_ports { camera_eye_r_xclk }];
set_property -dict { LOC AB14 IOSTANDARD LVCMOS33 } [get_ports { camera_eye_r_scl }];
set_property -dict { LOC AB13 IOSTANDARD LVCMOS33 } [get_ports { camera_eye_r_sda }];

set_property -dict { LOC L21 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_eye_r_clock_p }];
set_property -dict { LOC G19 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_eye_r_data_p[0] }];
set_property -dict { LOC J19 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_eye_r_data_p[1] }];
set_property -dict { LOC M18 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_eye_r_data_p[2] }];
set_property -dict { LOC P19 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_eye_r_data_p[3] }];

set_property -dict { LOC K21 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_eye_r_clock_n }];
set_property -dict { LOC F20 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_eye_r_data_n[0] }];
set_property -dict { LOC H20 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_eye_r_data_n[1] }];
set_property -dict { LOC M19 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_eye_r_data_n[2] }];
set_property -dict { LOC N19 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_eye_r_data_n[3] }];

set_property -dict { LOC A5 IOSTANDARD LVCMOS33 } [get_ports { camera_mouth_power_en }];
set_property -dict { LOC B5 IOSTANDARD LVCMOS33 } [get_ports { camera_mouth_xclk }];
set_property -dict { LOC B4 IOSTANDARD LVCMOS33 } [get_ports { camera_mouth_scl }];
set_property -dict { LOC A3 IOSTANDARD LVCMOS33 } [get_ports { camera_mouth_sda }];

set_property -dict { LOC C16 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_mouth_clock_p }];
set_property -dict { LOC B14 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_mouth_data_p[0] }];
set_property -dict { LOC A15 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_mouth_data_p[1] }];
set_property -dict { LOC D17 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_mouth_data_p[2] }];
set_property -dict { LOC A20 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_mouth_data_p[3] }];

set_property -dict { LOC B17 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_mouth_clock_n }];
set_property -dict { LOC B15 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_mouth_data_n[0] }];
set_property -dict { LOC A16 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_mouth_data_n[1] }];
set_property -dict { LOC C17 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_mouth_data_n[2] }];
set_property -dict { LOC A21 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_mouth_data_n[3] }];

set_property -dict { LOC D1 IOSTANDARD LVCMOS33 } [get_ports { camera_pass_l_power_en }];
set_property -dict { LOC D2 IOSTANDARD LVCMOS33 } [get_ports { camera_pass_l_xclk }];
set_property -dict { LOC D3 IOSTANDARD LVCMOS33 } [get_ports { camera_pass_l_scl }];
set_property -dict { LOC B3 IOSTANDARD LVCMOS33 } [get_ports { camera_pass_l_sda }];

set_property -dict { LOC E13 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_pass_l_clock_p }];
set_property -dict { LOC B12 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_pass_l_data_p[0] }];
set_property -dict { LOC B13 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_pass_l_data_p[1] }];
set_property -dict { LOC C14 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_pass_l_data_p[2] }];
set_property -dict { LOC E15 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_pass_l_data_p[3] }];

set_property -dict { LOC D13 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_pass_l_clock_n }];
set_property -dict { LOC A12 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_pass_l_data_n[0] }];
set_property -dict { LOC A13 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_pass_l_data_n[1] }];
set_property -dict { LOC C15 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_pass_l_data_n[2] }];
set_property -dict { LOC E16 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_pass_l_data_n[3] }];

set_property -dict { LOC Y15 IOSTANDARD LVCMOS33 } [get_ports { camera_pass_r_power_en }];
set_property -dict { LOC W14 IOSTANDARD LVCMOS33 } [get_ports { camera_pass_r_xclk }];
set_property -dict { LOC V15 IOSTANDARD LVCMOS33 } [get_ports { camera_pass_r_scl }];
set_property -dict { LOC U15 IOSTANDARD LVCMOS33 } [get_ports { camera_pass_r_sda }];

set_property -dict { LOC J21 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_pass_r_clock_p }];
set_property -dict { LOC N21 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_pass_r_data_p[0] }];
set_property -dict { LOC M22 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_pass_r_data_p[1] }];
set_property -dict { LOC H22 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_pass_r_data_p[2] }];
set_property -dict { LOC F21 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_pass_r_data_p[3] }];

set_property -dict { LOC J22 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_pass_r_clock_n }];
set_property -dict { LOC N22 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_pass_r_data_n[0] }];
set_property -dict { LOC L22 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_pass_r_data_n[1] }];
set_property -dict { LOC G22 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_pass_r_data_n[2] }];
set_property -dict { LOC F22 IOSTANDARD MIPI_DPHY_DCI_HS } [get_ports { camera_pass_r_data_n[3] }];

set_property -dict { LOC V12 IOSTANDARD LVCMOS33 } [get_ports { track_clock[0] }];
set_property -dict { LOC W11 IOSTANDARD LVCMOS33 } [get_ports { track_clock[1] }];
set_property -dict { LOC Y11 IOSTANDARD LVCMOS33 } [get_ports { track_clock[2] }];
set_property -dict { LOC Y9 IOSTANDARD LVCMOS33 } [get_ports { track_clock[3] }];
set_property -dict { LOC W13 IOSTANDARD LVCMOS33 } [get_ports { track_clock[4] }];
set_property -dict { LOC U14 IOSTANDARD LVCMOS33 } [get_ports { track_clock[5] }];
set_property -dict { LOC Y14 IOSTANDARD LVCMOS33 } [get_ports { track_clock[6] }];
set_property -dict { LOC AA12 IOSTANDARD LVCMOS33 } [get_ports { track_clock[7] }];
set_property -dict { LOC V2 IOSTANDARD LVCMOS33 } [get_ports { track_clock[8] }];
set_property -dict { LOC T1 IOSTANDARD LVCMOS33 } [get_ports { track_clock[9] }];
set_property -dict { LOC R1 IOSTANDARD LVCMOS33 } [get_ports { track_clock[10] }];
set_property -dict { LOC P1 IOSTANDARD LVCMOS33 } [get_ports { track_clock[11] }];
set_property -dict { LOC AA5 IOSTANDARD LVCMOS33 } [get_ports { track_clock[12] }];
set_property -dict { LOC AA3 IOSTANDARD LVCMOS33 } [get_ports { track_clock[13] }];
set_property -dict { LOC AA1 IOSTANDARD LVCMOS33 } [get_ports { track_clock[14] }];
set_property -dict { LOC W1 IOSTANDARD LVCMOS33 } [get_ports { track_clock[15] }];

set_property -dict { LOC W9 IOSTANDARD LVCMOS33 } [get_ports { track_data[0] }];
set_property -dict { LOC W12 IOSTANDARD LVCMOS33 } [get_ports { track_data[1] }];
set_property -dict { LOC Y10 IOSTANDARD LVCMOS33 } [get_ports { track_data[2] }];
set_property -dict { LOC Y8 IOSTANDARD LVCMOS33 } [get_ports { track_data[3] }];
set_property -dict { LOC Y13 IOSTANDARD LVCMOS33 } [get_ports { track_data[4] }];
set_property -dict { LOC V14 IOSTANDARD LVCMOS33 } [get_ports { track_data[5] }];
set_property -dict { LOC AA13 IOSTANDARD LVCMOS33 } [get_ports { track_data[6] }];
set_property -dict { LOC AB12 IOSTANDARD LVCMOS33 } [get_ports { track_data[7] }];
set_property -dict { LOC U2 IOSTANDARD LVCMOS33 } [get_ports { track_data[8] }];
set_property -dict { LOC T2 IOSTANDARD LVCMOS33 } [get_ports { track_data[9] }];
set_property -dict { LOC R3 IOSTANDARD LVCMOS33 } [get_ports { track_data[10] }];
set_property -dict { LOC P2 IOSTANDARD LVCMOS33 } [get_ports { track_data[11] }];
set_property -dict { LOC AA5 IOSTANDARD LVCMOS33 } [get_ports { track_data[12] }];
set_property -dict { LOC AA2 IOSTANDARD LVCMOS33 } [get_ports { track_data[13] }];
set_property -dict { LOC Y1 IOSTANDARD LVCMOS33 } [get_ports { track_data[14] }];
set_property -dict { LOC V1 IOSTANDARD LVCMOS33 } [get_ports { track_data[15] }];

set_property -dict { LOC W3 IOSTANDARD LVPECL } [get_ports { external_clock_p }];
set_property -dict { LOC W2 IOSTANDARD LVPECL } [get_ports { external_clock_n }];

create_clock -period 2.0 -name external_clock [get_ports { external_clock }];
